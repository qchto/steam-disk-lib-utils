# Steam Disk Library Manager v.0.5 (beta)

Interactive bash script to manage Steam libraries from the Linux CLI independently from Steam itself.

[![](usage.gif)](http://www.youtube.com/watch?v=xJooQGdRRjY "Yes, it can run Crysis! (technically)")

# IMPORTANT

- The file [`steamlibmanager.sh`](https://gitlab.com/qchto/steam-disk-lib-utils/blob/master/steamlibmanager.sh) is the main script.
- The file [`steamlibmanager.conf`](https://gitlab.com/qchto/steam-disk-lib-utils/blob/master/steamlibmanager.conf) is the config file that need to be adapted accordingly (recommended to be copied into `$HOME/.config/steamlibmanager.conf`).
- **It's important the variable `STEAM_BASE_LIB_PATH` is correctly set in this file**. If not, this script assume your Steam default library is located under `$HOME/.local/share/Steam`, and in case of it not being a valid location, you may be prompted to modify it.
- **This script may create a file `.steam_disk_libs` with the relative path to the libraries contained in the root path of each partition** for the refered libraries to be easily restorable, but you can manage these manually (i.e. a steam library with `steamapps` under `/mnt/drive/games/steamlib/steamapps` contained in a drive mounted in `/mnt/drive` will require the line `games/steamlib` in a `/mnt/drive/.steam_disk_libs` file).\*
- **This script can work in parallel or independently of Steam running, although it's recommended to avoid working with libraries actively used by Steam**. This script is set to check, validate and limit the access to currently active Steam Libraries and proceed accordingly.
- [`steamlibmanager.sh`](https://gitlab.com/qchto/steam-disk-lib-utils/blob/master/steamlibmanager.sh) can receive a set of parameters that refers libraries to work with, it will also load Steam Active Libraries dynamically at start.
- [`steamlibmanager.sh`](https://gitlab.com/qchto/steam-disk-lib-utils/blob/master/steamlibmanager.sh) script uses a custom "one line menu" selector, please report any issue you find with it.
- Precautions have been taken so it is mostly safe to interrupt this script with *<Ctrl+C>*, but doing so during an operation (moving, copying, removing, regenerating configs, etc) can risk corrupting the involved files (which are reported individually).
- After usage, if using SDs or non-Linux formated drives (ntfs, vfat, f2fs), **take care of first unmounting loop files refered to inside them before unmounting and ejecting these devices**.
- Although these have been extensively tested in some distros (mostly Arch and Debian based) with no adverse results, **this script is still an beta version!** Make sure you read the warning at the beginning. USE WITH CAUTION AT YOUR OWN DISCRETION!!!

## Features

- Get Steam libraries up and running in seconds. Play any game in them ASAP.
- Find repeated games between all your libraries in seconds. [Recover hundreds of GBs in minutes.](https://youtu.be/8UOpWutDNic).
- Quick referal and indexation of libraries per partition (creating a flagfile).
- Copies, moves and removals of games in active or inactive libraries (independently -although aware- of Steam running or not in the background).
- Copies and moves (if in external drives) syncronized to destinations, so take less time.
- Moves (if in the same drive) and removals takes seconds.
- Validation and Preparation of devices where libraries are located (internal and external, in multiple formats: ext2/3/4, vfat, ntfs, btrfs, xfs, f2fs) to be used directly by Steam.
- Enhanced performance and reduce disk usage of external devices dependent in compatdata folder (mostly Proton games).
- Portability of Steam games and libraries from/to other PCs (independent of OS).
- Seamless backups of games.
- Quick Management of games specific folders (Shaders and CompatData)
- Quick revision of Game information with popular related sites (Steam, ProtonDB, SteamDB and PCGamingWiki).
- Automatic download and installation of popular Compatibility Tools: Proton-GE, Boxtron and Roberta (credits to [patrickm32](https://github.com/patrickm32/proton-ge-custom-updater)\*\* for the base of this implementation)
- Manual requests to force compile of shaders for games (experimental, using Steam fossilize executable and fozpipelinesv4 shadercache directory paths by default).

## Usage

### without installation (recommended for GamerOS users)

- Download this repo.
- Copy `steamlibmanager.conf` into `$HOME/.config/`.\*\*\*
- Set up the file `$HOME/.config/steamlibmanager.conf`(especifically, the variable `STEAM_BASE_LIB_PATH` if not correctly set).
 - GamerOS users may prefer to replace `steamlibmanager.conf` with `steamlibmanager_gameros.conf` for a preset configuration enabling access to running libraries out-of-the-box.
- Run `steamlibmanager.sh` from a terminal as a command (optionally passing libraries folders as attributes). i.e. `./steam-disk-lib-utils/steamlibmanager.sh /full/path/to/inactive/steamlibrary relative/path/to/a/steamlib /collection/of/libs*`
- Enjoy.

### with installation

- Follow installation procedure (check below, this process can be applied each time after upgrading repo).
- Modify `$HOME/.config/steamlibmanager.conf` accordingly.\*\*\*
- Run from any launcher (or terminal) using the command `steamlibmanager` (libraries can still be passed as parameters).
- Enjoy.

## Installation (Optional)
```sh
git clone https://gitlab.com/qchto/steam-disk-lib-utils.git
cd steam-disk-lib-utils
sudo make install
cp steamlibmanager.conf $HOME/.config/ # can be ommited with config file already in place from a previous installation
```

## Uninstallation
```sh
cd steam-disk-lib-utils
sudo make uninstall
```

## Known bugs

- Flatpak versions are currently being tested, so be warned:
  - The reported process id file (steam.pid) doesn't refer to a local process, so it's simply referencial and can't be used to monitor the status of steam from a regular terminal.
  - When an external library is refered, a permission is override in the flatpak itself to allow access to its path, but no effort is done latter to automatically remove this permission. If you consider this a security breach, avoid the addition of libraries altogether. If you have a suggestion on how to treat this, let me know by reporting an issue.
- Some untreated escape characters (like \\r) have broken display in the past, but most cases have been already considered. Still, if this happen to you, it is recommended to stop any procedure involving this game and report it as an issue to be treated.
- Special characters in game install directory names may affect operations (e.g. previously affected games with "&", ";", "(" and ")" in the title, like in the case of "Tomb Raider (I)", but no longer the case). If you find a game that present has this problem using the latest version of the script, please report it as an issue.
- If a corrupted manifest file exists in a refered library, it can result in misbehavior (although a lot of validations have been set in place to avoid damage). In this cases usualy an read/write error is thrown in-screen. If this is your case revise the library and try to remove/recover the corrupted file manually OR avoid using the affected library altogether.
- For non-intensive block devices (like SD cards or old USB2 thumbdrives), the extended usage of this script (or any read/write operation in general) can result in hardware damage, so it is recommended not to have more than 1 library in these devices and avoid using it for disk intensive games (although games like "Crysis", "GTAV" and "Resident Evil 3" have been succesfully launched from these devices, but it's not recommended).
- Avoid using windows formatted drives (NTFS, exFAT, fat32) for installing Linux native games as Steam may simply refuse (or fail) to use the drive for some.
- Some old formats (like fat32) may simply not work for big games due to the FS limitations. If your device use this and can't be reformatted, be aware that Steam may simply fail to use it (may report read/write errors). Still, some success cases for this devices are old DOS games (e.g. "Descent") and some newer smaller games (e.g. "Sonic Mania").

---
\*refer to [`the image above`](https://gitlab.com/qchto/steam-disk-lib-utils/blob/master/usage.gif) for a visual example of script usage and interface in action.

\*\*if you only want to keep Proton-GE up to date, this script is more straight-forward.

\*\*\*make sure to revise these variables, some functionalities may not work properly if they are not correctly set.
