#!/bin/bash

#non-configurable variables
SCRIPTTITLE="Steam Disk Library Manager v.0.5.1"

# color pallete
COLOR_RED='\e[0;31m'
COLOR_GREEN='\e[0;32m'
COLOR_YELLOW='\e[0;33m'
COLOR_BLUE='\e[0;34m'
COLOR_MAGENTA='\e[0;35m'
COLOR_CYAN='\e[0;36m'
COLOR_RESET='\e[0m'
COLOR_HRED='\e[1;31m'
# shellcheck disable=SC2034  # Unused variable left for completion
COLOR_HGREEN='\e[1;32m'
COLOR_HYELLOW='\e[1;33m'
COLOR_HBLUE='\e[1;34m'
COLOR_HMAGENTA='\e[1;35m'
COLOR_HCYAN='\e[1;36m'
COLOR_HRESET='\e[1m'
COLOR_WRED='\e[4;31m'
COLOR_WGREEN='\e[4;32m'
# shellcheck disable=SC2034  # Unused variable left for completion
COLOR_WYELLOW='\e[4;33m'
# shellcheck disable=SC2034  # Unused variable left for completion
COLOR_WBLUE='\e[4;34m'
# shellcheck disable=SC2034  # Unused variable left for completion
COLOR_WMAGENTA='\e[4;35m'
# shellcheck disable=SC2034  # Unused variable left for completion
COLOR_WCYAN='\e[4;36m'
COLOR_WRESET='\e[4m'

DEFAULT_LINELENGTH=100

echo -e "${COLOR_WGREEN}$SCRIPTTITLE${COLOR_RESET}"

# init section
[ "$EUID" -eq 0 ] && echo "This script must NOT be run as root. Exiting." >&2 && exit 1

#preset configurable variables
# shellcheck source=/dev/null # Sourced `steamlibmanager.conf` file may vary location
if [ -f "$HOME/.config/steamlibmanager.conf" ]
then
 . "$HOME/.config/steamlibmanager.conf"
 echo -e "${COLOR_BLUE}Loaded config file in [${COLOR_HBLUE}$HOME/.config/steamlibmanager.conf${COLOR_BLUE}].${COLOR_RESET}\n"
elif [ -f "$(dirname "$0")/steamlibmanager.conf" ]
then
 . "$(dirname "$0")/steamlibmanager.conf"
 echo -e "${COLOR_BLUE}Loaded config file in [${COLOR_HBLUE}$(dirname "$0")/steamlibmanager.conf${COLOR_BLUE}].${COLOR_RESET}\n"
fi

#ensure the script is executed in a terminal environment
if [ ! -t 0 ]
then
 [ -z "$TERMINALCMD" ] && TERMINALCMD="xterm -e"
 echo "This script must be run from a terminal." >&2

 passedcmd="$(realpath "$0")"
 passedcmd="${passedcmd//\\/\\\\\\\\}"
 passedcmd="${passedcmd//\"/\\\\\\\"}"
 passedcmd="${passedcmd//;/\\;}"
 passedcmd="${passedcmd// /\\ }"
 for param in "$@"
 do
  param="${param//\\/\\\\\\\\}"
  param="${param//\"/\\\\\\\"}"
  param="${param//;/\\;}"
  param="${param// /\\ }"
  passedcmd="$passedcmd $param"
 done

 ( $TERMINALCMD "$passedcmd" ) & disown
 exit 0
fi

#initialize all missing environmental configurable variables
isflatpak=false && [ -n "$STEAM_FLATPAKID" ] && isflatpak=true
[ -z "$ASK_IF_STEAM_RUNNING" ] && ASK_IF_STEAM_RUNNING=true
[ -z "$PROCEED_STEAM_RUNNING" ] && PROCEED_STEAM_RUNNING=false
[ -z "$CACHE_FLAG_FILE" ] && CACHE_FLAG_FILE=.steam_disk_libs
[ -z "$COMPATDATAMOUNT_FILE" ] && COMPATDATAMOUNT_FILE=compatdata.mount
[ -z "$COMPATDATAMOUNT_FILE_LOCAL" ] && COMPATDATAMOUNT_FILE_LOCAL=$HOME/compatdatacache.mount
[ -z "$COMPATDATACACHE_PATH" ] && COMPATDATACACHE_PATH=$HOME/compatdatacache
[ -z "$ALLOWTMP_COMPATDATAMOUNT_FILE" ] && ALLOWTMP_COMPATDATAMOUNT_FILE=true
[ -z "$ALLOWTMP_COMPATDATACACHE_PATH" ] && ALLOWTMP_COMPATDATACACHE_PATH=true
if $isflatpak
then
 [ -z "$STEAMPID_CHECKER" ] && STEAMPID_CHECKER="flatpak ps | grep \"$steam_pid\""
else
 [ -z "$STEAMPID_CHECKER" ] && STEAMPID_CHECKER="ps --no-headers \$steam_pid"
fi
[ -z "$STEAM_BASE_LIB_PATH" ] && STEAM_BASE_LIB_PATH=$HOME/.local/share/Steam
[ -z "$STEAM_COMPATIBILITYTOOLS_PATH" ] && STEAM_COMPATIBILITYTOOLS_PATH="$STEAM_BASE_LIB_PATH/compatibilitytools.d"
[ -z "$STEAM_SHADERCOMPILER" ] && STEAM_SHADERCOMPILER="$STEAM_BASE_LIB_PATH/ubuntu12_64/fossilize_replay"
[ -z "$STEAM_PID_PATH" ] && STEAM_PID_PATH=$HOME/.steam/steam.pid
[ -z "$STEAMPID_HEARTBEAT" ] && STEAMPID_HEARTBEAT=2
[ -z "$STEAMPID_RETRIES" ] && STEAMPID_RETRIES=10
[ -z "$RELOAD_REPOS_EVERY" ] && RELOAD_REPOS_EVERY=3600
[ -z "$URL_BROWSER" ] && URL_BROWSER=steambrowser
[ -z "$DIR_BROWSER" ] && DIR_BROWSER=xdg-open
[ -z "$USB_VERSION_LOWTHRESHOLD" ] && USB_VERSION_LOWTHRESHOLD=3.0

steamconfigpath="$STEAM_BASE_LIB_PATH/config/config.vdf"
steamlibconfigpath="$STEAM_BASE_LIB_PATH/steamapps/libraryfolders.vdf"

# disclaimer section
echo -e "${COLOR_HRED}WARNING!!${COLOR_RESET} This script is still in ${COLOR_WRESET}beta${COLOR_RESET}!"
echo ""
echo -e "${COLOR_HBLUE}DISCLAIMER:${COLOR_RESET}"
echo -e " This script, at user resquest:"
echo -e " - Will search through all your mounted partitions the libraries to the declared ones in files [${COLOR_HBLUE}<partition_path>/${COLOR_HMAGENTA}$CACHE_FLAG_FILE${COLOR_RESET}] and can create and modify these in each partition you refer libraries from."
echo -e " - May modify your steam library folders located under the paths refered by files [${COLOR_HBLUE}<partition_path>/${COLOR_HMAGENTA}$CACHE_FLAG_FILE${COLOR_RESET}] or refered through parameters."
echo -e " - May modify (after backup) your steam config files located under [${COLOR_HBLUE}$steamconfigpath${COLOR_RESET}] and [${COLOR_HBLUE}$steamlibconfigpath${COLOR_RESET}] to reset any custom libraries selected."
echo -e " - May request (but never store) your password if a super-user procedure is requested by the user."
echo -e " - Will ${COLOR_WGREEN}log everything in-screen${COLOR_RESET}, so make sure to keep an eye on it!"
echo -e "${COLOR_WRED}USE WITH CAUTION AT YOUR OWN DISCRETION${COLOR_RESET}"
echo ""

# check for dependencies available and set vars accordingly
# module loop (flag: mountloop will be used later)
depmodloop=false
modinfo loop &>/dev/null && depmodloop=true #kernel module present
! $depmodloop && grep -q "loop" "/lib/modules/$(uname -r)/modules.builtin" && depmodloop=true #kernel module is built-in
! $depmodloop && echo "No loop module found, mount of loop files will be disabled" >&2
# command perl (flag depcmdperl)
depcmdperl=false
command -v perl &>/dev/null && depcmdperl=true
! $depcmdperl && echo "No command perl found, some issues with onelinemenu sizing may arise" >&2
# command rsync (flag depcmdrsync)
depcmdrsync=false
command -v rsync &>/dev/null && depcmdrsync=true
! $depcmdrsync && echo "No command rsync found, some operations will be unavailable" >&2
depcmdcurl=false
command -v curl &>/dev/null && depcmdcurl=true
! $depcmdcurl && echo "No command curl found, some operations will be unavailable" >&2
depcmddirbrowser=false
command -v "${DIR_BROWSER%% *}" &>/dev/null && depcmddirbrowser=true
! $depcmddirbrowser && echo "No valid DIR_BROWSER command set, some operations will be unavailable" >&2
depcmdurlbrowser=false
command -v "${URL_BROWSER%% *}" &>/dev/null && depcmdurlbrowser=true
[ "$URL_BROWSER" = "steambrowser" ]  && depcmdurlbrowser=true
! $depcmdurlbrowser && echo "No valid URL_BROWSER command set, some operations will be unavailable" >&2
depcmdfossilize=false
command -v "$STEAM_SHADERCOMPILER" &>/dev/null && depcmdfossilize=true
! $depcmdfossilize && echo "No valid STEAM_SHADERCOMPILER command set, some operations will be unavailable" >&2

# get user data
echo -e "${COLOR_BLUE}Data of user executing this script${COLOR_RESET}"
user_uuid="$(id -u "$USER")"
user_ugid="$(id -g "$USER")"
echo -e "${COLOR_MAGENTA}> Username:          $USER${COLOR_RESET}"
echo -e "${COLOR_MAGENTA}> User UUID:         $user_uuid${COLOR_RESET}"
echo -e "${COLOR_MAGENTA}> User Main Group:   $user_ugid${COLOR_RESET}"

# init traps and prepare environment
function trap_basic () {
 trap - SIGINT SIGTERM
 lasttrap="-"
}
function trap_donothing () {
 trap donothing SIGINT SIGTERM
 lasttrap="donothing"
}
function trap_forcedexit () {
 trap forcedexit SIGINT SIGTERM
 lasttrap="forcedexit"
}
function trap_forcedgameopbreak () {
 trap forcedgameopbreak SIGINT SIGTERM
 lasttrap="forcedgameopbreak"
}
function untrap () {
 trap - SIGINT SIGTERM
}
function trap_restorelasttrap () {
 # shellcheck disable=SC2064 # "$lasttrap" is meant to be set at the moment of the call
 trap "$lasttrap" SIGINT SIGTERM
}
lasttrap=""
trap_forcedexit
trap cleanexit exit

# function section
function stty_regulate () {
 stty -echo #disallows user input to be shown in the terminal to avoid visual cluttering
}
function stty_sane () {
 stty sane #return output to normal
}

function forcedgameopbreak () {
 echo "User interrupted operation." >&2
 echo ""
 echo -ne "${COLOR_RED}Procedure cancelled ...${COLOR_RESET}"
 while :
 do 
  echo -ne "${COLOR_HRESET} Cancel all following procedures in operation queue? [y/n/a${COLOR_RESET}(nd for all other procedures)${COLOR_HRESET}/]${COLOR_RESET}"
  read -r -N 1 -p " > " cancelall </dev/tty
  if [ "$cancelall" = "y" ]
  then
   confirmed="n" #next operation loop will count as "no" (finish current procedure, ask for next one)
   break
  elif [ "$cancelall" = "n" ]
  then
   confirmed="y" #next on procedure loop count as still "yes" (proceed with next item)
   break
  elif [ "$cancelall" = "a" ]
  then
   confirmed="c" #next on procedure and operation loop will count as "cancel all" (finish operation)
   break
  else
   echo -ne '\r'"${COLOR_YELLOW}Press [y], [n] or [a] only.${COLOR_RESET}"
  fi
 done
 echo ""
}
function donothing () {
  echo -n ""
}
function forcedexit () {
 echo "User finalized script." >&2
 exit 1
}
function cleanexit () {
 stty_sane
 revoke_sudo
 echo ""
 echo -n "Goodbye! (Press any key to exit)"
 read -r -s -N 1 _ </dev/tty
 echo ""
 exit 0
}
function ask_for_sudo () {
 prefix="$1"
 
 cansudo=false
 untrap
 sudo -p " Sudo Password:" echo -e "\r${prefix}${COLOR_GREEN} Sudoer Authorized${COLOR_RESET}" && cansudo=true || echo -e "\r${prefix}${COLOR_RED} Sudoer Unauthorized${COLOR_RESET}"
 trap_restorelasttrap
 return
}
function revoke_sudo () {
 sudo -k
}
function sizedline () {
 size="$1"
 char=" "
 [ -n "$2" ] && char="$2"

 line=""
 for ((ichar=0 ; ichar<size ; ichar++))
 do
  line="$line$char"
 done
 echo "$line"
}
function drawline () {
 linelength=$(tput cols 2>/dev/null) #try using the whole available screen width
 [ -z "$linelength" ] && linelength=$DEFAULT_LINELENGTH
 fullline="$(sizedline "$linelength" "-")"
 echo -e "\n${COLOR_HMAGENTA}$fullline${COLOR_RESET}"
}
function countspecialchars () {
 char="$1"
 #TODO find a better portable way to count special characters width.
 if $depcmdperl
 then
  perl -CSAD -E '$_ = shift; $count++ for /./g, /\p{Ea=W}/g; say $count' "$char"
 else
  # shellcheck disable=SC2000 # `echo | wc -c` is the hack used to determine character length
  [[ "$(echo "$char" | wc -c)" -ge 4 ]] && echo 2 && return #dismiss shellcheck: using `wc -c` to count bytes, not chars
  echo 1
 fi
}
function coloredstringcounter () {
 string="$1"
 indexstart="$2"
 subslen="$3"
 
 string=${string//\\x1b/\\e} ; string=${string//\\033/\\e} #normalize escape characters to \e
 counting=true
 pre_e=true
 pre_bracket=true
 counter=0
 substring=""
 lastcolor=""
 copymode=false
 for (( c=0 ; c<${#string} ; c++ ))
 do
  $counting && pre_e=true && pre_bracket=true #reinit if counting
  char="${string:$c:1}"
  charw=1
  # shellcheck disable=SC1003 # I mean a backslash '\'
  case "$char" in
   '\')
    if $counting
    then
     counter=$((counter +1)) # still counts until escaped : \e
     counting=false
    elif ! $pre_e || ! $pre_bracket
    then
     counting=true
     ! $copymode && lastcolor=""
    else
     counter=$((counter +1))
     counting=true
     ! $copymode && lastcolor=""
    fi
    ;;
   'e')
    if $counting
    then
     counter=$((counter +1))
    else
     counter=$((counter -1)) #uncount \ : \e
     pre_e=false
     ! $copymode && lastcolor="\e" #if not defined as escaped character, is lost later
    fi
    ;;
   '[')
    if $pre_e || ! $pre_bracket
    then
     counter=$((counter +1))
     counting=true
    else
     pre_bracket=false
     ! $copymode && lastcolor="$lastcolor$char"
    fi
    ;;
   ';'|[0-9])
    if $counting
    then
     counter=$((counter +1))
    elif $pre_e
    then
     counter=$((counter +1))
     counting=true
    else
     ! $copymode && lastcolor="$lastcolor$char"
    fi
    ;;
   [[:ascii:]]) #speed hack, avoid count of special chars completely from ASCII chars
    if $counting || $pre_e
    then
      counter=$((counter +1))
    else
     ! $copymode && lastcolor="$lastcolor$char"
    fi
    counting=true
    ;;
   *)
    if $counting || $pre_e
    then
     charw=$(countspecialchars "$char")
     counter=$((counter + charw))
    else
     ! $copymode && lastcolor="$lastcolor$char"
    fi
    counting=true
    ;;
  esac
  ! $copymode && [ -n "$indexstart" ] && [[ $indexstart -lt $counter ]]  && copymode=true && ! [[ $indexstart -eq $((counter - charw)) ]] && char="…" #only copy complete characters, show trail if counted partially
  [ -n "$subslen" ] && [[ $((indexstart + subslen)) -lt $counter ]] && counter=$((counter - charw)) && break #all done, ready to print
  $copymode && substring="$substring$char"
 done
 substring="$lastcolor$substring\e[0m"

}
function countcharsonly () {
 string="$1"
 
 coloredstringcounter "$string" #with 1 parameter, it simply counts
 echo $counter
}
function coloredsubstring () {
 string="$1"
 indexstart="$2"
 subslen="$3"
 
 coloredstringcounter "$string" "$indexstart" "$subslen" #with 3 parameters, it retrieves a substring
 echo "$substring"
}

function onelinemenu () {
 selopt=$1
 prompt="$2"
 options="$3"
 options="${options//\\r/\\\\r}" #escape \r of options
 escapable=true
 [ -n "$4" ] && [ "$4" = "false" ] && escapable=false
 inputs=""
 cursor=1
 helptxt=$(cat <<HELPTXT
------- One Line Menu Help: --------------------------------------------------------------------------------------------
          [ Enter ] - selects currently shown option *(unavailable if no option is given)
	    [ Esc ] - exit without selecting an option *(can be deactivated)
 [ Tab ] / [ Down ] - next option
             [ Up ] - previous option
          [ Right ] - forward 5 options
           [ Left ] - back 5 options
              [ ? ] - display help and list of currently available options  (press [ Esc ]+[ q ] to exit this screen)
	Type - search for an option (matching text)
v------ Below this point are the currently available options (use [ / ] and type a name to search ) -------------------v
HELPTXT
 )
 while true
 do
  linelength=$(tput cols 2>/dev/null) #try using the whole available screen width
  [ -z "$linelength" ] && linelength=$DEFAULT_LINELENGTH

  grepinputs="$inputs"
  grepinputs="${grepinputs//\\/\\\\}" #escape \
  grepinputs="${grepinputs//\[/\\\[}" #escape [
  avoptions="$(echo "$options" | grep -i -- "$grepinputs")"
  option="$(echo "$avoptions" | head -$cursor | tail -1)"
  avno=0
  [ -n "$avoptions" ] && avno="$(echo "$avoptions" | wc -l)"
  menuline="${COLOR_MAGENTA}$prompt [${COLOR_GREEN}${inputs//\\/\\\\}${COLOR_MAGENTA}] ($cursor/$avno) : ${COLOR_CYAN}$option${COLOR_RESET}"
  curlen="$(countcharsonly "$menuline")"
  if [[ $curlen -gt $linelength ]]
  then
   menuline="…$(coloredsubstring "$menuline" $((curlen - linelength +1)) $((linelength -1)) )" 
  else
   menuline="${menuline}$(sizedline "$((linelength-curlen))")"
  fi
  echo -ne "\r$menuline"
  read -r -s -N 1 input </dev/tty
  case "$input" in
   $'\x7f') #backspace
    [ -n "$inputs" ] && inputs="${inputs::-1}"
    cursor=1
    ;;
   $'\x09') #tab
    cursor=$((cursor+1)) && [[ $cursor -gt $avno ]] && cursor=1
    ;;
   $'\x1b'|'['|'O') #escape sequence (or repeated escape sequence)
    read -r -s -N 1 -t .1 stub </dev/tty
    if [ "$stub" = "" ]
    then
     if [ "$input" = $'\x1b' ] #esc
     then
      $escapable && option="" && break
     else #another single printable character
      inputs="$inputs$input"
      cursor=1
     fi
    fi
    { [ "$stub" = "[" ] || [ "$stub" = "O" ] ; } && read -r -s -N 1 stub </dev/tty
    case "$stub" in
     A) # up
      cursor=$((cursor-1)) && [[ $cursor -lt 1 ]] && cursor=$avno
      ;;
     B) # down
      cursor=$((cursor+1)) && [[ $cursor -gt $avno ]] && cursor=1
      ;;
     C) # right
      cursor=$((cursor+5)) && [[ $cursor -gt $avno ]] && cursor=1
      ;;
     D) # left
      cursor=$((cursor-5)) && [[ $cursor -lt 1 ]] && cursor=$avno
      ;;
    esac
    read -r -s -N 4 -t .1 </dev/tty # flush out other sequences
    ;;
   $'\x0a') #enter
    [ -n "$option" ] && break
    ;;
   '?')
    echo -e "$helptxt\n$avoptions" | less
    ;;
   [[:graph:]]|' ')
    inputs="$inputs$input"
    cursor=1
    ;;
  esac
 done
 emptyline="$(sizedline "$linelength")"
 echo -ne "\r$emptyline\r"
 declare -g "$selopt"="$option"
}

function islinuxfs () {
 fsformat="$1"
 
 [[ "$fsformat" =~ ^"ext"[2|3|4]$|^"tmpfs"$|^"xfs"$|^"btrfs"$ ]] && return 0 #true
 return 1 #false
}
function islimitedfs () {
 fsformat="$1"
 
 [[ "$fsformat" =~ ^"vfat"$ ]] && return 0 #true
 return 1 #false
}
function islowperfdrive () {
 devicepath="$1"
 
 #make sure devicepath refers to a block device (in case of loopfile is passed as device)
 devicepath="$(df "$devicepath" | tail -1 | awk '{print $1}')"
 
 #validate if SD
 [[ "$devicepath" =~ ^"/dev/mmc" ]] && lowperfreason="SD card" && return 0 #true
 #validate USB version
 usbver="" && \
  usbver=$(ID_BUS="" && eval "$(udevadm info -q property --export "$devicepath" )" && [ "$ID_BUS" = "usb" ] && 2>/dev/null lsusb -vv -d "$ID_VENDOR_ID:$ID_MODEL_ID" | grep bcdUSB | awk '{print $2}') && \
  [[ "$usbver" =~ ^[0-9]+(\.[0-9]+)?$ ]] && (( $(echo "$usbver < $USB_VERSION_LOWTHRESHOLD" | bc -l) )) && lowperfreason="USB $usbver device" && return 0 #true
 lowperfreason="" && return 1 #false
}
function getdiruserandgroup () {
 passeddir="$1"
 
 # shellcheck disable=SC2010 # This method uses `ls -a | grep ...` to determine ownership of the directory
 [ -d "$passeddir" ] && ls -an "$passeddir" | grep " \."$ | awk '{print $3" "$4}' && return
 echo ""
}
function formatbytes () {
 bytes=$1
 LC_ALL=c numfmt --to iec --format "%0.2f" "$bytes"
}
function formatunits () {
 units=$1
 LC_ALL=c numfmt --from iec --format "%0f" "$units"
}
function formatbytes_i () {
 bytes=$1
 LC_ALL=c numfmt --to iec-i --format "%0.2f" "$bytes"
}
function formatunits_i () {
 units=$1
 LC_ALL=c numfmt --from iec-i --format "%0f" "$units"
}
function formattime () {
 giventime=$1
 minutes=$(( giventime / 60 ))
 seconds=$(( giventime % 60 ))
 [[ $seconds -lt 10 ]] && seconds="0$seconds"
 echo "$minutes:$seconds"
}

function validatesteamlib () {
 steamlib="$1"
 verbose=false
 interactive=false
 [ "$2" = "true" ]  && verbose=true
 [ "$3" = "true" ]  && interactive=true

 usedsteamlib=false #flag - unused library unless function passes a certain point (currently being used by Steam Linux)
 validsourcelib=false #flag - invalid source library unless function passes a certain point (can't be used by the script)
 validsteamlib=false #flag - invalid destination library unless function passes a certain point (can't be used by the script)
 preparedsteamlib=false #flag - unprepared library unless function passes a certain point (shouldn't be used by Steam Linux)

 $verbose && echo -e "${COLOR_BLUE}----------------------------------------------${COLOR_RESET}"
 $verbose && echo -e "${COLOR_BLUE}Steam Library [${COLOR_MAGENTA}$steamlib${COLOR_BLUE}]:${COLOR_RESET}"
 
 warning=""
 error=""

 # validate if  truly a steam library (steamapps dir present)
 if [ ! -d "$steamlib/steamapps" ]
 then
  error="Not a Steam Library. Can't be used."
  { $verbose || $interactive ; } && echo -e "${COLOR_RED}> $error${COLOR_RESET}" || echo "$error" >&2
  $verbose && echo -e "${COLOR_BLUE}----------------------------------------------${COLOR_RESET}"
  return
 fi

 #detemine if the library is being used
 if issteamrunning && { echo -e "$activelibs" | grep -q ^"$steamlib"$ ;}
 then
  usedsteamlib=true
  { $verbose || $interactive ; } && echo -e "${COLOR_YELLOW}> Library is currently being used by Steam. Some limitations may apply${COLOR_RESET}"
 fi

 # retrieve important data from directory
 steampartdfline="$(df "$steamlib" | tail -1)"
 steampartdev="$(echo "$steampartdfline" | awk '{print $1}')"
 [[ "$steampartdev" =~ "/dev/loop"* ]] && steampartdev="$(losetup --list "$steampartdev" | tail -1 | awk '{for(i=6;i<=(NF-2);i++) printf("%s%s", $i, i == NF - 2 ? "\n" : OFS)}')"
 steampart="$(echo "$steampartdfline" | awk '{$1=$2=$3=$4=$5="" ; print $0}' | xargs)"
 steampartline="$(mount | grep "$steampartdev" | grep "$steampart")"
 steampartformat="${steampartline% (*}" ; steampartformat="${steampartformat#* type }"
 $verbose && echo -e "${COLOR_BLUE}> Library partition path [${COLOR_MAGENTA}$steampart${COLOR_BLUE}] in device [${COLOR_MAGENTA}$steampartdev${COLOR_BLUE}] with format [${COLOR_HCYAN}$steampartformat${COLOR_BLUE}].${COLOR_RESET}"
 
 validsourcelib=true #This library can already be used as source!
 
 read -r sluser slgroup <<<"$(getdiruserandgroup "$steamlib")"
 if [ "$sluser" != "$user_uuid" ] || [ "$sluser" != "$user_uuid" ]
 then
  error="Steam library ownership is not assigned to current user."
  if $interactive
  then # try to change permissions of the folder
   echo -e "${COLOR_YELLOW}> $error${COLOR_RESET}"
   echo -ne "${COLOR_HYELLOW}>> Take ownership of [${COLOR_MAGENTA}$steamlib${COLOR_HYELLOW}] and its contents?${COLOR_HRESET}[y/n]${COLOR_RESET}"
   read -r -N 1 -p " > " chownconfirm </dev/tty
   if [ "$chownconfirm" = "y" ]
   then
    echo -e "${COLOR_GREEN} Accepted${COLOR_RESET}"
    sudo_prefix="${COLOR_HYELLOW}>> Ownership change requires superuser ... ${COLOR_RESET}"
    echo -ne "$sudo_prefix"
    ask_for_sudo "$sudo_prefix"
    if $cansudo
    then
     sudo chown -R "$user_uuid:$user_ugid" "$steamlib"
     revoke_sudo
    fi
   else
    echo -e "${COLOR_RED} Cancelled.${COLOR_RESET}"
   fi
   # shellcheck disable=SC2034  # Unused variables left for readability
   read -r sluser slgroup <<<"$(getdiruserandgroup "$steamlib")"
   if [ "$sluser" = "$user_uuid" ] || [ "$sluser" = "$user_uuid" ] #revalidate
   then
    echo -e "${COLOR_GREEN}>> Ownership Changed${COLOR_RESET}"
   else
    echo -e "${COLOR_RED}>> Ownership Not Changed${COLOR_RESET}"
    revalidate=false
   fi
  fi
  if [ "$sluser" != "$user_uuid" ] || [ "$sluser" != "$user_uuid" ] #revalidate just in case
  then # cannot proceed until permissions are changed
   error="$error Cannot be used as destination."
   ! $interactive && error="$error Please prepare the library first."
   { $verbose || $interactive ; } && echo -e "${COLOR_RED}> $error${COLOR_RESET}" || echo "$error" >&2
   { $verbose || $interactive ; } && echo -e "${COLOR_BLUE}----------------------------------------------${COLOR_RESET}"
   return
  fi
 fi
 
 relativelib="${steamlib#$steampart/}"
 
 # validate existance in $CACHE_FLAG_FILE
 revalidate=true
 if ! [ -f "$steampart/$CACHE_FLAG_FILE" ]
 then
  warning="No $CACHE_FLAG_FILE found."
  ! $interactive && warning="$warning Steam Library reference be lost after usage unless added." && revalidate=false
  { $verbose || $interactive ; } && echo -e "${COLOR_YELLOW}> $warning${COLOR_RESET}" 
  if $interactive
  then
   # interactive create "$steampart/$CACHE_FLAG_FILE" 
   echo -ne "${COLOR_HYELLOW}>> Create file [${COLOR_MAGENTA}$steampart/${COLOR_BLUE}$CACHE_FLAG_FILE${COLOR_HYELLOW}]?${COLOR_HRESET}[y/n]${COLOR_RESET}"
   read -r -N 1 -p " > " interconfirm </dev/tty
   if [ "$interconfirm" = "y" ]
   then
    echo -e "${COLOR_GREEN} Accepted${COLOR_RESET}"
    if ! { touch "$steampart/$CACHE_FLAG_FILE" ; } &>/dev/null
    then
     sudo_prefix="${COLOR_HYELLOW}>> File creation requires superuser ... ${COLOR_RESET}"
     echo -ne "$sudo_prefix"
     ask_for_sudo "$sudo_prefix"
     if $cansudo
     then
      sudo touch "$steampart/$CACHE_FLAG_FILE"
      sudo chown "$user_uuid:$user_ugid" "$steampart/$CACHE_FLAG_FILE"
      revoke_sudo
     fi
    fi
   else
    echo -e "${COLOR_RED} Cancelled.${COLOR_RESET}"
   fi
   if [ -f "$steampart/$CACHE_FLAG_FILE" ]
   then
    echo -e "${COLOR_GREEN}>> File Created${COLOR_RESET}"
   else
    echo -e "${COLOR_RED}>> File Not Created${COLOR_RESET}"
    revalidate=false
   fi
  fi 
 fi
 if $revalidate && [ -f "$steampart/$CACHE_FLAG_FILE" ] && ! { grep -q ^"$relativelib"$ "$steampart/$CACHE_FLAG_FILE" ;}
 then
  warning="Steam Library reference not in $CACHE_FLAG_FILE."
  ! $interactive && warning="$warning Will be lost after usage unless added." && revalidate=false
  { $verbose || $interactive ; } && echo -e "${COLOR_YELLOW}> $warning${COLOR_RESET}" 
  if $interactive
  then
   # interactive add $relativelib to "$steampart/$CACHE_FLAG_FILE"
   echo -ne "${COLOR_HYELLOW}> Add library to file [${COLOR_MAGENTA}$steampart/${COLOR_BLUE}$CACHE_FLAG_FILE${COLOR_HYELLOW}]?${COLOR_HRESET}[y/n]${COLOR_RESET}"
   read -r -N 1 -p " > " interconfirm </dev/tty
   if [ "$interconfirm" = "y" ]
   then
    echo -e "${COLOR_GREEN} Accepted${COLOR_RESET}"
    if ! { echo "$relativelib" >> "$steampart/$CACHE_FLAG_FILE" ; } &>/dev/null
    then
     sudo_prefix="${COLOR_HYELLOW}>> File modification requires superuser ... ${COLOR_RESET}"
     echo -ne "$sudo_prefix"
     ask_for_sudo "$sudo_prefix"
     if $cansudo
     then
      sudo tee -a "$steampart/$CACHE_FLAG_FILE" <<<"$relativelib"
      revoke_sudo
     fi
    fi
   else
    echo -e "${COLOR_RED} Cancelled.${COLOR_RESET}"
   fi
   if { grep -q ^"$relativelib"$ "$steampart/$CACHE_FLAG_FILE" ;}
   then
    echo -e "${COLOR_GREEN}>> Library Added${COLOR_RESET}"
   else
    echo -e "${COLOR_RED}>> Library Not Added${COLOR_RESET}"
    revalidate=false
   fi
  fi 
 fi
 if $revalidate && [ -f "$steampart/$CACHE_FLAG_FILE" ] && { grep -q ^"$relativelib"$ "$steampart/$CACHE_FLAG_FILE" ;}
 then
  $verbose && echo -e "${COLOR_BLUE}> Library exists with relative path in [${COLOR_MAGENTA}$relativelib${COLOR_BLUE}] in partition cache file [${COLOR_MAGENTA}$steampart/$CACHE_FLAG_FILE${COLOR_BLUE}].${COLOR_RESET}"
 fi

 # validate format
 requiresexecperm=false
 requirescompatdata=false
 supportlinking=true
 case $steampartformat in
  'vfat')
   requiresexecperm=true ; [[ "$steampartline" = *"fmask=0000"* ]] && [[ "$steampartline" = *"dmask=0000"* ]] && requiresexecperm=false
   requirescompatdata=true
   supportlinking=false
   ;;
  'fuseblk')
   requirescompatdata=true
   supportlinking=false ; ( testln="$steampart/tmp_$RANDOM" && ln -s /tmp "$testln" && rm "$testln" ) &>/dev/null && supportlinking=true
   ;;
  'f2fs')
   requirescompatdata=true
   ;;
  *)
   if ! { islinuxfs "$steampartformat" ;}
   then
    error="Library drive format is not recognized as a usable FS. Will only be used as source."
    { $verbose || $interactive ; } && echo -e "${COLOR_RED}> $error${COLOR_RESET}" || echo "$error" >&2
    $verbose && echo -e "${COLOR_BLUE}----------------------------------------------${COLOR_RESET}"
    return
   fi
   ;;
 esac
 if $requiresexecperm
 then
  # validate exec permissions for partition (remount if possible and revalidate)
  if $interactive
  then
   warning="Library drive requires to be remounted with exec permissions."
   echo -e "${COLOR_YELLOW}> $warning${COLOR_RESET}"
   sudo_prefix="${COLOR_HYELLOW}>> Superuser required to remount partition ... ${COLOR_RESET}"
   echo -ne "$sudo_prefix"
   ask_for_sudo "$sudo_prefix"
   if $cansudo
   then
    sudo umount "$steampart" && \
    ( [ -d "$steampart" ] && mkdir -p "$steampart" || sudo mkdir -p "$steampart" ) && \
    sudo mount -o uid="$user_uuid",gid="$user_ugid",umask=000 "$steampartdev" "$steampart"
    revoke_sudo
   fi
   steampartline="$(mount | grep "$steampartdev" | grep "$steampart")"
   if [[ "$steampartline" = *"fmask=0000"* ]] && [[ "$steampartline" = *"dmask=0000"* ]] 
   then
    echo -e "${COLOR_GREEN}>> Library Remounted with exec permissions${COLOR_RESET}"
    requiresexecperm=false
   else
    echo -e "${COLOR_RED}>> Library Not Remounted with exec permissions${COLOR_RESET}"
   fi
  fi
  if $requiresexecperm
  then
   error="Library drive currently doesn't allow exec permissions. Can't be used."
   { $verbose || $interactive ; } && echo -e "${COLOR_RED}> $error${COLOR_RESET}" || echo "$error" >&2
   $verbose && echo -e "${COLOR_BLUE}----------------------------------------------${COLOR_RESET}"
   return
  fi
 fi

 # validate drive type and partition format for library performance
 lowperfdevice=""
 lowcompatdata=false
 limitedformat=""
 limitedcompatdata=false
 { islowperfdrive "$steampartdev" ;} && lowperfdevice="$steampartdev: $lowperfreason" && lowcompatdata=true
 { islimitedfs "$steampartformat" ;} && limitedformat="$steampartformat" && limitedcompatdata=true

 # validate compatdata folder
 compatdatalinkexists=false
 compatdataformat=""
 compatdataexists=false
 [ -L "$steamlib/steamapps/compatdata" ] && compatdatalinkexists=true && compatdataformat="na"
 if [ -d "$steamlib/steamapps/compatdata" ]
 then
  compatdataexists=true
  compatdatadfline="$(df "$steamlib/steamapps/compatdata" | tail -1)"
  compatdatapartdev="$(echo "$compatdatadfline" | awk '{print $1}')"
  [[ "$compatdatapartdev" =~ "/dev/loop"* ]] && compatdatapartdev="$(losetup --list "$compatdatapartdev" | tail -1 | awk '{for(i=6;i<=(NF-2);i++) printf("%s%s", $i, i == NF - 2 ? "\n" : OFS)}')"
  compatdatapart="$(echo "$compatdatadfline" | awk '{$1=$2=$3=$4=$5="" ; print $0}' | xargs)"
  compatdatapartline="$(mount | grep "$compatdatapartdev" | grep "$compatdatapart")"
  compatdataformat="${compatdatapartline% (*}" ; compatdataformat="${compatdataformat#* type }"
  { islowperfdrive "$compatdatapartdev" ;} && lowcompatdata=true || lowcompatdata=false
  { islimitedfs "$compatdataformat" ;} && limitedcompatdata=true
  if { islinuxfs "$compatdataformat" ;}
  then
   requirescompatdata=false
   { $verbose || $interactive ; } && echo -e "${COLOR_GREEN}> This library's compatfile folder is already refered to a valid formatted partition.${COLOR_RESET}"
  fi
 fi
 { $verbose || $interactive ; } && [ -n "$lowperfdevice" ] && echo -e "${COLOR_BLUE}> Library located in a low performance device [${COLOR_HCYAN}$lowperfdevice${COLOR_BLUE}].${COLOR_RESET}"
 { $verbose || $interactive ; } && [ -n "$limitedformat" ] && echo -e "${COLOR_BLUE}> Library located in a limited formatted partition [${COLOR_HCYAN}$limitedformat${COLOR_BLUE}].${COLOR_RESET}"
 # validate drive type and partition format for compatdata
 if [ -n "$lowperfdevice$limitedformat" ] # validates: $lowprefdevice || $limitedformat
 then
  { $verbose || $interactive ; } && echo -ne "${COLOR_BLUE}> To enhance performance, using an external compatdata mount/link is recommended."
  if $lowcompatdata
  then
   $interactive &&  echo -ne " You will be prompted with options to do so."
  else
   { $verbose || $interactive ; } && echo -ne "... ${COLOR_GREEN}Already done!"
  fi
  { $verbose || $interactive ; } && echo -e "${COLOR_RESET}"
 fi
 if $requirescompatdata || $lowcompatdata || $limitedcompatdata
 then
  # validate compatdata folder (create and/or mount/link compatdata folder if necessary/possible)
  { $verbose || $interactive ; } && { $lowcompatdata || $limitedcompatdata ; } && echo -e "${COLOR_YELLOW}> Library drive exists in a limited device or partition. To enhance performance, it's recommended to use with an externally refered compatdata folder (either mounted or linked).${COLOR_RESET}"
  $requirescompatdata && error="Library drive currently doesn't allow permissions needed to run games. Can't be used with Steam Linux as is."
  if $depmodloop && $interactive
  then
   #loop module available, let's try
   [ -n "$error" ] && echo -e "${COLOR_YELLOW}> $error${COLOR_RESET}"
   echo -e "${COLOR_YELLOW}> A compatdata folder with a valid format may need to be mounted.${COLOR_RESET}"

   compatdatamountfiles=""
   ! $lowcompatdata && ! $limitedcompatdata && compatdatamountfiles="$compatdatamountfiles$steamlib/$COMPATDATAMOUNT_FILE\n$steampart/$COMPATDATAMOUNT_FILE\n" #don't risk creating a mountfile in an low perf. dev. (as SD card) or under a limited FS, dd overload has fried a couple of mine, not worth the risk! if you want to use it for compatdata, simply format it to linux fs (disable this validation at your own risk).
   [ -n "$COMPATDATAMOUNT_FILE_LOCAL" ] && compatdatamountfiles="$compatdatamountfiles$COMPATDATAMOUNT_FILE_LOCAL\n"
   $ALLOWTMP_COMPATDATAMOUNT_FILE && compatdatamountfiles="$compatdatamountfiles/tmp/$COMPATDATAMOUNT_FILE\n"
   compatdatamountfiles="${compatdatamountfiles%\\n}"
   compatdatamountfile=""
   compatdatasize=""
   if [ -z "$compatdatamountfiles" ]
   then
    echo -e "${COLOR_YELLOW}> No compatdata folders available to be linked.${COLOR_RESET}"
   else
    while [ -z "$compatdatamountfile" ]
    do
     compatdatasize=""
     echo -ne "${COLOR_HYELLOW}> Compatdata Mountfile to be mounted. Proceed?${COLOR_HRESET} [y/n]${COLOR_RESET}"
     opconfirm=""
     read -r -N 1 -p " > " opconfirm </dev/tty
     ! [ "$opconfirm" = "y" ] && echo "" && break
     if $compatdatalinkexists
     then
      echo -ne "\r${COLOR_HYELLOW}> Current compatdata library folder link exists, will need to be moved. Proceed?${COLOR_HRESET} [y/n]${COLOR_RESET}"
      opconfirm=""
      read -r -N 1 -p " > " opconfirm </dev/tty
      ! [ "$opconfirm" = "y" ] && echo "" && break
     fi
     onelinemenu compatdatafile "> Select a Compatdata Mountfile to Use" "$(echo -e "$compatdatamountfiles")" true
     # shellcheck disable=SC2154 # `compatdatafile` dinamically set by onelinemenu
     [ -z "$compatdatafile" ] && continue
     if [ -d "$compatdatafile" ]
     then
      echo -e "${COLOR_YELLOW}> Compatdata Mountfile is a directory!${COLOR_RESET}" && continue
     elif [ -f "$compatdatafile" ]
     then
      compatdatamountfile="$compatdatafile"
     else
      echo -ne "${COLOR_HYELLOW}> Compatdata Mountfile [${COLOR_MAGENTA}$compatdatafile${COLOR_HYELLOW}] doesn't exist. Create it?${COLOR_HRESET} [y/n]${COLOR_RESET}"
      opconfirm=""
      read -r -N 1 -p " > " opconfirm </dev/tty
      if [ "$opconfirm" = "y" ]
      then
       onelinemenu compatdatasz "> Select a Compatdata Mountfile size to create" "$(echo -e "512M\n1.0G\n5.0G\n10G\n20G")" true
       # shellcheck disable=SC2154 # `compatdatasz` dinamically set by onelinemenu
       [ -n "$compatdatasz" ] && compatdatasize="$(( $(formatunits "$compatdatasz") /1024 /1024))" && compatdatamountfile="$compatdatafile"
      fi
     fi
    done
   fi
   if [ -n "$compatdatamountfile" ]
   then
    createdisclaimer=""
    [ -n "$compatdatasize" ] && createdisclaimer=" created (size=$(formatbytes "$((compatdatasize *1024 *1024))")) and"
    echo -e "${COLOR_YELLOW}>> Compatdata mountfile [${COLOR_MAGENTA}$compatdatamountfile${COLOR_YELLOW}] selected to be$createdisclaimer mounted.${COLOR_RESET}"
    sudo_prefix="${COLOR_HYELLOW}>> Superuser required to proceed ... ${COLOR_RESET}"
    echo -ne "$sudo_prefix"
    ask_for_sudo "$sudo_prefix"
    proceed=false
    if $cansudo
    then
     proceed=true
     $proceed && $compatdatalinkexists && proceed=false && movesufix="OLD_$(date +%Y%m%d_%H%M%S)" && sudo mv "$steamlib/steamapps/compatdata" "$steamlib/steamapps/compatdata$movesufix" && compatdataexists=false && proceed=true && echo -e "${COLOR_YELLOW}>> Existing compatdata link moved to [${COLOR_MAGENTA}$steamlib/steamapps/compatdata$movesufix${COLOR_YELLOW}].${COLOR_RESET}"
     $proceed && ! $compatdataexists && proceed=false && sudo mkdir "$steamlib/steamapps/compatdata" && compatdataexists=true && proceed=true && echo -e "${COLOR_YELLOW}>> Missing compatdata folder created at [${COLOR_MAGENTA}$steamlib/steamapps/compatdata${COLOR_YELLOW}] created.${COLOR_RESET}"
     $proceed && [ -n "$compatdatasize" ] && proceed=false && sudocreatefile "${COLOR_YELLOW}> Creating [$compatdatamountfile] ... ${COLOR_RESET}" "$compatdatasize" "$compatdatamountfile" && sudo mkfs.ext4 "$compatdatamountfile" && sudo chown "$user_uuid:$user_ugid" "$compatdatamountfile" && proceed=true
     $proceed && proceed=false && sudo mount "$compatdatamountfile" "$steamlib/steamapps/compatdata" && sudo chown -R "$user_uuid:$user_ugid" "$steamlib/steamapps/compatdata" && proceed=true
     revoke_sudo
    fi
    if $proceed
    then
     compatdatadfline="$(df "$steamlib/steamapps/compatdata" | tail -1)"
     compatdatapartdev="$(echo "$compatdatadfline" | awk '{print $1}')"
     [[ "$compatdatapartdev" =~ "/dev/loop"* ]] && compatdatapartdev="$(losetup --list "$compatdatapartdev" | tail -1 | awk '{for(i=6;i<=(NF-2);i++) printf("%s%s", $i, i == NF - 2 ? "\n" : OFS)}')"
     compatdatapart="$(echo "$compatdatadfline" | awk '{$1=$2=$3=$4=$5="" ; print $0}' | xargs)"
     compatdatapartline="$(mount | grep "$compatdatapartdev" | grep "$compatdatapart")"
     compatdataformat="${compatdatapartline% (*}" ; compatdataformat="${compatdataformat#* type }"
     { islinuxfs "$compatdataformat" ;} && requirescompatdata=false
     { islowperfdrive "$compatdatapartdev" ;} && lowcompatdata=true || lowcompatdata=false
     { islimitedfs "$compatdataformat" ;} && limitedcompatdata=true
    fi
   fi
   if $requirescompatdata
   then
    echo -e "${COLOR_RED}>> Compatdata mountfile [${COLOR_MAGENTA}$compatdatamountfile${COLOR_RED}] not mounted.${COLOR_RESET}"
   elif $lowcompatdata || $limitedcompatdata
   then
    if { islinuxfs "$compatdataformat" ;}
    then
     echo -e "${COLOR_YELLOW}>> Compatdata is still in a limited device or partition. It's usable, but performance may be affected.${COLOR_RESET}"
    else
     echo -e "${COLOR_RED}>> Compatdata is in an incompatible format using a limited device or partition. Isn't usable.${COLOR_RESET}"
    fi
   else
    echo -e "${COLOR_GREEN}>> Compatdata mountfile [${COLOR_MAGENTA}$compatdatamountfile${COLOR_GREEN}]$createdisclaimer mounted.${COLOR_RESET}"
   fi
  elif [ -n "$error" ]
  then
   # let this pass as a valid library for now, as we could still softlink
   { $verbose || $interactive ; } && echo -e "${COLOR_RED}> $error${COLOR_RESET}" || echo "$error" >&2
  fi
  if $requirescompatdata || $lowcompatdata || $limitedcompatdata
  then
   if $supportlinking && $interactive
   then
    #try softlinking
    echo -e "${COLOR_YELLOW}> A compatdata folder with a valid format may need to be linked.${COLOR_RESET}"
    compatdatamountlinks=""
    [ -n "$COMPATDATACACHE_PATH" ] && compatdatamountlinks="$compatdatamountlinks$COMPATDATACACHE_PATH\n"
    $ALLOWTMP_COMPATDATACACHE_PATH && compatdatamountlinks="$compatdatamountlinks/tmp/compatdata\n"
    compatdatamountlinks="${compatdatamountlinks%\\n}"
    compatdatamountlink=""
    createcompatdatalinkdir=false
    if [ -z "$compatdatamountlinks" ]
    then
     echo -e "${COLOR_YELLOW}> No compatdata folders available to be linked.${COLOR_RESET}"
    else
     while [ -z "$compatdatamountlink" ]
     do
      echo -ne "${COLOR_HYELLOW}> Compatdata folder to be linked. Proceed?${COLOR_HRESET} [y/n]${COLOR_RESET}"
      opconfirm=""
      read -r -N 1 -p " > " opconfirm </dev/tty
      ! [ "$opconfirm" = "y" ] && echo "" && break
      if $compatdatalinkexists || $compatdataexists
      then
       echo -ne "\r${COLOR_HYELLOW}> Current compatdata library folder/link exists, will need to be moved. Proceed?${COLOR_HRESET} [y/n]${COLOR_RESET}"
       opconfirm=""
       read -r -N 1 -p " > " opconfirm </dev/tty
       ! [ "$opconfirm" = "y" ] && echo "" && break
      fi
      onelinemenu compatdatalink "> Select a Compatdata Folder to Use" "$(echo -e "$compatdatamountlinks")" true
      # shellcheck disable=SC2154 # `compatdatalink` dinamically set by onelinemenu
      [ -z "$compatdatalink" ] && continue
      if [ -f "$compatdatalink" ]
      then
       echo -e "${COLOR_YELLOW}> Compatdata Folder [${COLOR_MAGENTA}$compatdatalink${COLOR_HYELLOW}] path is a file!${COLOR_RESET}" && continue
      elif [ -d "$compatdatalink" ]
      then
       compatdatamountlink="$compatdatalink"
      else
       echo -ne "${COLOR_HYELLOW}> Compatdata Folder [${COLOR_MAGENTA}$compatdatalink${COLOR_HYELLOW}] doesn't exist. Create it?${COLOR_HRESET} [y/n]${COLOR_RESET}"
       opconfirm=""
       read -r -N 1 -p " > " opconfirm </dev/tty
       if [ "$opconfirm" = "y" ]
       then
        createcompatdatalinkdir=true
        compatdatamountlink="$compatdatalink"
       fi
       echo -ne "\r"
      fi
     done
    fi
    if [ -n "$compatdatamountlink" ]
    then
     createdisclaimer=""
     $createcompatdatalinkdir && createdisclaimer=" created and"
     echo -e "${COLOR_YELLOW}>> Compatdata folder [${COLOR_MAGENTA}$compatdatamountlink${COLOR_YELLOW}] selected to be$createdisclaimer linked.${COLOR_RESET}"
     sudo_prefix="${COLOR_HYELLOW}>> Superuser required to proceed ... ${COLOR_RESET}"
     echo -ne "$sudo_prefix"
     ask_for_sudo "$sudo_prefix"
     proceed=false
     if $cansudo
     then
      proceed=true
      $proceed && { $compatdatalinkexists || $compatdataexists ; } && proceed=false && movesufix="OLD_$(date +%Y%m%d_%H%M%S)" && sudo mv "$steamlib/steamapps/compatdata" "$steamlib/steamapps/compatdata$movesufix" && compatdataexists=false && proceed=true && echo -e "${COLOR_YELLOW}>> Existing compatdata folder moved to [${COLOR_MAGENTA}$steamlib/steamapps/compatdata$movesufix${COLOR_YELLOW}].${COLOR_RESET}"
      $proceed && $createcompatdatalinkdir && proceed=false && sudo mkdir -p "$compatdatamountlink" && proceed=true && echo -e "${COLOR_YELLOW}>> Created linkable compatdata folder in [${COLOR_MAGENTA}$compatdatamountlink${COLOR_YELLOW}].${COLOR_RESET}"
      $proceed && proceed=false && sudo ln -s "$compatdatamountlink" "$steamlib/steamapps/compatdata" && sudo chown -R "$user_uuid:$user_ugid" "$compatdatamountlink" && proceed=true
      revoke_sudo
     fi
     if $proceed
     then
      compatdatadfline="$(df "$steamlib/steamapps/compatdata" | tail -1)"
      compatdatapartdev="$(echo "$compatdatadfline" | awk '{print $1}')"
      [[ "$compatdatapartdev" =~ "/dev/loop"* ]] && compatdatapartdev="$(losetup --list "$compatdatapartdev" | tail -1 | awk '{for(i=6;i<=(NF-2);i++) printf("%s%s", $i, i == NF - 2 ? "\n" : OFS)}')"
      compatdatapart="$(echo "$compatdatadfline" | awk '{$1=$2=$3=$4=$5="" ; print $0}' | xargs)"
      compatdatapartline="$(mount | grep "$compatdatapartdev" | grep "$compatdatapart")"
      compatdataformat="${compatdatapartline% (*}" ; compatdataformat="${compatdataformat#* type }"
      { islinuxfs "$compatdataformat" ;} && requirescompatdata=false
      { islowperfdrive "$compatdatapartdev" ;} && lowcompatdata=true || lowcompatdata=false
      { islimitedfs "$compatdataformat" ;} && limitedcompatdata=true
     fi
    fi
    if $requirescompatdata
    then
     echo -e "${COLOR_RED}>> Compatdata folder [${COLOR_MAGENTA}$compatdatamountlink${COLOR_RED}] not linked.${COLOR_RESET}"
    elif $lowcompatdata || $limitedcompatdata
    then
     if { islinuxfs "$compatdataformat" ;}
     then
      echo -e "${COLOR_YELLOW}>> Compatdata is still in a limited device or partition. It's usable, but performance may be affected.${COLOR_RESET}"
     else
      echo -e "${COLOR_RED}>> Compatdata is in an incompatible format using a limited device or partition. Isn't usable.${COLOR_RESET}"
     fi
    else
     echo -e "${COLOR_GREEN}>> Compatdata folder [${COLOR_MAGENTA}$compatdatamountlink${COLOR_GREEN}]$createdisclaimer linked.${COLOR_RESET}"
    fi
   elif ! $supportlinking
   then
    error="Library doesn't support softlinking. Can't be used safely as destination"
    { $verbose || $interactive ; } && echo -e "${COLOR_RED}> $error${COLOR_RESET}" || echo "$error" >&2
    $verbose && echo -e "${COLOR_BLUE}----------------------------------------------${COLOR_RESET}"
    return
   fi
  fi
 fi
 if { $compatdataexists || [ -n "$compatdataformat" ] ; } && ! { islinuxfs "$compatdataformat" ;}
 then
  error="Library compatdata folder not formatted properly. Can't be used safely as destination"
  { $verbose || $interactive ; } && echo -e "${COLOR_RED}> $error${COLOR_RESET}" || echo "$error" >&2
  $verbose && echo -e "${COLOR_BLUE}----------------------------------------------${COLOR_RESET}"
  return
 fi
 if { $verbose || $interactive ; } #show warnings if necessary
 then
  ! { islinuxfs "$steampartformat" ;} && echo -e "${COLOR_YELLOW}> ${COLOR_HRED}WARNING!! Do NOT install any version of Proton in the listed libraries for this partition or games that require it will not run.${COLOR_HYELLOW} If Steam downloads Proton in this library, move it to another ext formated location.${COLOR_RESET}"
  { islimitedfs "$steampartformat" ;} && echo -e "${COLOR_YELLOW}> ${COLOR_HRED}WARNING!! This partition present LIMITATIONS in file capacity, so it's possible some games will simply fail to download/install in it.${COLOR_HYELLOW} If Steam presents a read/write problem with games in this library, move it to another partition's library.${COLOR_RESET}"
 fi
 
 validsteamlib=true #flag - valid library as function is completed
 if ! $requirescompatdata 
 then
   # if after all this, the compatdata problem has been resolved, the library is prepared to be used by Steam Linux
   preparedsteamlib=true #flag - prepared library as function is completed
   { $verbose || $interactive ; } && echo -e "${COLOR_GREEN}> Library ready to be used by Steam Linux.${COLOR_RESET}"
 fi
 $verbose && echo -e "${COLOR_BLUE}----------------------------------------------${COLOR_RESET}"
}

function progressbar () {
 prefix="$1"
 backgroundcmd="$2"
 totalcmd="$3"
 progresscmd="$4"
 #from $5 on parameters can be added only to be interpreted for commands passed
 
 completechar="■"
 incompletechar="·"
 currentchar="$incompletechar"
 
 linelength=$(tput cols 2>/dev/null) #try using the whole available screen width
 [ -z "$linelength" ] && linelength=$DEFAULT_LINELENGTH
 pfxseparator=""
 pfxseplen=${#pfxseparator}
 prefixlen="$(countcharsonly "$prefix")"
 length=$(( linelength - prefixlen - pfxseplen))
 refperc=0
 
 cpstderrlog="/tmp/cpstderrlog$RANDOM"
 cpsize=$(eval "$totalcmd")
 (
  eval "$backgroundcmd"
 ) 2>$cpstderrlog 1>/dev/null &
 bgpid=$!
 pcolor="${COLOR_RED}"
 [[ $cpsize -eq 0 ]] && refperc=100 #preset as full if copying 0 size files
 tsstart=$(date +%s)
 while true
 do
  cpcursize=0
  cpcursize=$(eval "$progresscmd")
  [[ $cpsize -gt 0 ]] && refperc=$((100* cpcursize / cpsize))
  [[ $refperc -gt 0 ]] && pcolor="${COLOR_YELLOW}" && [[ $refperc -ge 100 ]] && refperc=100
  tscurrent=$(date +%s)
  timepassed=$((tscurrent - tsstart))
  timeestimated="0"
  [[ $refperc -ge 1 ]] && [[ $timepassed -gt 0 ]] && timeestimated=$(( timepassed * (100 - refperc) / refperc ))

  prebar="$(formatbytes "$((cpcursize *1024))")/$(formatbytes "$((cpsize *1024))") $(formattime $timepassed) $(formattime $timeestimated) "
  barlen=$(( length - ${#prebar} - ${#refperc} - 4 )) # 4 chars ="[] %"
  [[ $barlen -gt 0 ]] && charl=$((1000* 100 / barlen))
  bar=""
  char="$completechar"
  [ "$currentchar" = "$completechar" ] && currentchar="$incompletechar" || currentchar="$completechar"
  isdone=true
  for j in $(seq 2 $barlen) #count from two helps adding animated currentchar
  do
   $isdone && [[ $refperc -lt $((charl * j / 1000)) ]] && bar="$bar$currentchar" && char="$incompletechar" && isdone=false
   bar="$bar$char"
  done
  $isdone && bar="$bar$currentchar" #if the whole bar is completed a character will be missing, this adds it
  echo -ne "\r$prefix$pfxseparator$prebar${pcolor}[$bar] $refperc%${COLOR_RESET}"
  kill -0 $bgpid &>/dev/null || break
  sleep 0.5
 done
 stderrlog="$(cat "$cpstderrlog")"
 if [ -z "$stderrlog" ]
 then
  donecolor="${COLOR_GREEN}"
  donetext="Done in $(formattime $timepassed)."
 else
  donecolor="${COLOR_RED}"
  donetext="Exited at $refperc% in $(formattime $timepassed)."
 fi
 emptybar=""
 for j in $(seq 1 $(( length - ${#donetext} )) )
 do
  emptybar="${emptybar} "
 done
 echo -ne "\r$prefix$pfxseparator$donecolor$donetext${COLOR_RESET}$emptybar"
 [ -n "$stderrlog" ] && echo "Command executed with errors: $(eval echo "$backgroundcmd")" >&2 && echo "------ Command error log ------" >&2 && echo "$stderrlog" >&2 && echo "---- / Command error log ------" >&2 && return 1
 bgpid=""
 return 0
}
function createfile () {
 prefix="$1"
 size="$2"
 dstpath="$3"
 
 [ -e "$dstpath" ] && echo "Target already exists.. aborting" >&2 && exit 1
 [ ! -d "$(dirname "$dstpath")" ] && echo "No destination folder for $dstpath.. aborting" >&2 && exit 1
 
 # $5=$size
 # $6=$dstpath
 backgroundcmd="2>/dev/null dd if=/dev/zero of=\"\$6\" bs=1M count=\"\$5\""
 totalcmd="echo \$((\"\$5\" * 1024))"
 progresscmd="2>/dev/null du -d0 \"\$6\" | tail -1 | awk '{print \$1}'"

 progressbar "$prefix" "$backgroundcmd" "$totalcmd" "$progresscmd" "$size" "$dstpath" || return 1
 return 0
}
function sudocreatefile () {
 prefix="$1"
 size="$2"
 dstpath="$3"
 
 [ -e "$dstpath" ] && echo "Target already exists.. aborting" >&2 && exit 1
 [ ! -d "$(dirname "$dstpath")" ] && echo "No destination folder for $dstpath.. aborting" >&2 && exit 1
 
 # $5=$size
 # $6=$dstpath
 backgroundcmd="2>/dev/null sudo dd if=/dev/zero of=\"\$6\" bs=1M count=\"\$5\""
 totalcmd="echo \$((\"\$5\" * 1024))"
 progresscmd="2>/dev/null sudo du -d0 \"\$6\" | tail -1 | awk '{print \$1}'"

 progressbar "$prefix" "$backgroundcmd" "$totalcmd" "$progresscmd" "$size" "$dstpath" || return 1
 return 0
}
function copyfile () {
 prefix="$1"
 cpsrc="$2"
 cpdst="$3"
 
 { [ ! -f "$cpsrc" ] && [ ! -d "$cpsrc" ] ; } && echo "No file/folder $cpsrc.. aborting" >&2 && exit 1
 [ ! -d "$(dirname "$cpdst")" ] && echo "No destination folder for $cpdst.. aborting" >&2 && exit 1
 [ "$cpdst" = "." ] && cpdst="$(basename "$cpsrc")"
 [ "$cpdst" = ".." ] && cpdst="../$(basename "$cpsrc")"
 [ -d "$cpsrc" ] && cpsrc="${cpsrc%%+(/)}/" #add a single trail slash for rsync to act properly
 [ -d "$cpdst" ] && cpdst="${cpdst%%+(/)}" #remove all trail slash for rsync to act properly
 
 # $5=$cpsrc
 # $6=$cpdst
 backgroundcmd="rsync -au \"\$5\" \"\$6\""
 totalcmd="2>/dev/null du -d0 \"\$5\" | tail -1 | awk '{print \$1}'"
 progresscmd="2>/dev/null du -d0 \"\$6\" | tail -1 | awk '{print \$1}'"

 progressbar "$prefix" "$backgroundcmd" "$totalcmd" "$progresscmd" "$cpsrc" "$cpdst" || return 1
 return 0
}

function copygamedata () {
 prefix="$1"
 cpsrc="$2"
 cpdst="$3"
 normgameinstall="$4"

 copyfile "$prefix" "$cpsrc" "$cpdst" || return 1
 return 0
} 
function movegamedata () {
 prefix="$1"
 mvsrc="$2"
 mvdst="$3"
 normgameinstall="$4"
 
 rmflag=true
 $normgameinstall && [ "$(basename "$(dirname "$mvsrc")")" = "common" ] && rmflag=false
 mvsrcmountdfline="$(df "$mvsrc" | tail -1)"
 mvsrcmountdev="$(echo "$mvsrcmountdfline" | awk '{print $1}')"
 [ -e "$mvdst" ] && mvdirdst="$mvdst" || mvdirdst="$(dirname "$mvdst")"
 mvdstmountdfline="$(df "$mvdirdst" | tail -1)"
 mvdstmountdev="$(echo "$mvdstmountdfline" | awk '{print $1}')"
 samedrive=false
 if [ "$mvsrcmountdev" = "$mvdstmountdev" ]
 then
  samedrive=true
  mvsrcmountpart="$(echo "$mvsrcmountdfline" | awk '{$1=$2=$3=$4=$5="" ; print $0}' | xargs)"
  mvdstmountpart="$(echo "$mvdstmountdfline" | awk '{$1=$2=$3=$4=$5="" ; print $0}' | xargs)"
  [ "${mvsrc#$mvsrcmountpart}" = "${mvdst#$mvdstmountpart}" ] && echo -e "${COLOR_GREEN}Same location. Nothing to do.${COLOR_RESET}" && return 0
 fi
 if $rmflag && $samedrive && [ ! -e "$mvdst" ]
 then
  # same drive, can be removed and doesn't exist, simply move
  prfailed=false
  mv "$mvsrc" "$mvdst" || prfailed=true
  $prfailed && echo -e "${COLOR_RED}Failed.${COLOR_RESET}" && return 1
  echo -e "${COLOR_GREEN}Done.${COLOR_RESET}"
 else
  # diff drive or already exists or can't be removed, copy source, then remove (if possible)
  copygamedata "$prefix" "$mvsrc" "$mvdst" "$normgameinstall" || return 1
  if $rmflag
  then
   rmprefix=" > Removing source after copy ... "
   echo -ne "$rmprefix"
   removegamedata "$rmprefix" "$mvsrc" "$normgameinstall" || return 1
  else
   echo -e " > ${COLOR_YELLOW}Source can't be removed, only copied.${COLOR_RESET}"
  fi
 fi
 return 0
}
function removegamedata () {
 prefix="$1"
 rmsrc="$2"
 normgameinstall="$3"
 
 rmflag=true
 $normgameinstall && [ "$(basename "$(dirname "$rmsrc")")" = "common" ] && rmflag=false
 if $rmflag
 then
  # can be removed, proceed
  prfailed=false
  rm -r "$rmsrc" || prfailed=true
  $prfailed && echo -e "${COLOR_RED}Failed.${COLOR_RESET}" && return 1
  echo -e "${COLOR_GREEN}Done.${COLOR_RESET}"
 else
  # can't be removed, alert
  echo -e "${COLOR_YELLOW}Shouldn't be removed.${COLOR_RESET}"
 fi
 return 0
}
function opendir () {
 prefix="$1"
 dirsrc="$2"
 normgameinstall="$3"
 
 [ ! -d "$dirsrc" ] && echo -e "${COLOR_RED}Directory doesn't exist.. Skipping.${COLOR_RESET}" && return 1
 1>/dev/null 2>/dev/null $DIR_BROWSER "$dirsrc" & disown
 echo -e "${COLOR_GREEN}Done.${COLOR_RESET}" || return 1
 return 0
}
function opensteambasedir () {
 prefix="${COLOR_BLUE}Opening Main Steam Directory [${COLOR_HCYAN}$STEAM_BASE_LIB_PATH${COLOR_BLUE}] ... ${COLOR_RESET}"
 echo -ne "\r$prefix"
 opendir "$prefix" "$STEAM_BASE_LIB_PATH" false || return 1
 return 0
}
function opencompattooldir () {
 prefix="${COLOR_BLUE}Opening Compatibility Tools Directory [${COLOR_HCYAN}$STEAM_COMPATIBILITYTOOLS_PATH${COLOR_BLUE}] ... ${COLOR_RESET}"
 echo -ne "\r$prefix"
 opendir "$prefix" "$STEAM_COMPATIBILITYTOOLS_PATH" false || return 1
 return 0
}
function openlink () {
 gameparams="$1"

 echo -e " > ${COLOR_CYAN}Opening Url Link [${COLOR_BLUE}$gameparams${COLOR_CYAN}].${COLOR_RESET}"
 $URL_BROWSER "$gameparams" || return 1
 return 0
}
function analyzelib () {
 librarypath="$1"
 normgameinstall="$2"

 echo "------- Contents info for $librarypath -------"
 du -h -d 3 "$librarypath"
 echo "------- Partition info for $librarypath -------"
 df -h "$librarypath"
 echo "----------------------------------------------"
}
function validatelib () {
 prefix="$1"
 librarypath="$2"
 normgameinstall="$3"
 
 echo -e "${COLOR_YELLOW}In progress.${COLOR_RESET}" #In-line operation, requires a status update.
 validatesteamlib "$librarypath" true
}
function preparelib () {
 prefix="$1"
 librarypath="$2"
 normgameinstall="$3"
 
 echo -e "${COLOR_YELLOW}In progress.${COLOR_RESET}" #In-line operation, requires a status update.
 validatesteamlib "$librarypath" true true
}

function operate_two_libs () {
 operation=$1
 oplabel=$2
 sourcepath=$3
 destinationpath=$4
 operationcmd=$5
 inlineoperation=$6
 normgameinstall=$7

 proceedop=true
 tmpprefix="$operation $oplabel ... "
 inlineoperationprefix="$tmpprefix"
 echo -n "$tmpprefix"
 if [ ! -d "$sourcepath" ] && [ ! -f "$sourcepath" ]
 then
  echo -e "${COLOR_YELLOW}Source not found.. skipping.${COLOR_RESET}"
  proceedop=false
 elif $oprequiredst && { [ -d "$destinationpath" ] || [ -f "$destinationpath" ] ; }
 then
  tmpprefix="${COLOR_YELLOW}Destination already exists.. merging..${COLOR_RESET}"
  inlineoperationprefix="$inlineoperationprefix$tmpprefix"
  echo -ne "$tmpprefix"
 fi
 if $proceedop
 then
  destpathesc=${destinationpath// /\\ }
  destpathesc=${destpathesc//\'/\\\'}
  destpathesc=${destpathesc//&/\\&}
  destpathesc=${destpathesc//;/\\;}
  destpathesc=${destpathesc//\(/\\\(}
  destpathesc=${destpathesc//)/\\)}
  destbasedir="$(eval ""dirname "${destpathesc}""")"
  if [ -n "$destbasedir" ] && [  ! -d "$destbasedir" ]
  then
   tmpprefix="${COLOR_YELLOW}Destination directory doesn't exist.. creating it..${COLOR_RESET}"
   inlineoperationprefix="$inlineoperationprefix$tmpprefix"
   echo -ne "$tmpprefix"
   mkdir -p "$destbasedir"
  fi
  if $inlineoperation
  then
    $operationcmd "$inlineoperationprefix" "$sourcepath" "$destinationpath" "$normgameinstall"
  else
    opresult=$($operationcmd "$sourcepath" "$destinationpath")
    echo -e "${COLOR_GREEN}Done.${COLOR_RESET}"
    [ -n "$opresult" ] && while read -r opline ; do echo "    > $opline" ; done<<<"$opresult"
  fi
 fi
}
function operate_one_lib () {
 operation=$1
 oplabel=$2
 sourcepath=$3
 operationcmd=$4
 inlineoperation=$5
 normgameinstall=$6

 proceedop=true
 tmpprefix="$operation $oplabel ... "
 inlineoperationprefix="$tmpprefix"
 echo -n "$tmpprefix"
 if [ ! -d "$sourcepath" ] && [ ! -f "$sourcepath" ]
 then
  echo -e "${COLOR_YELLOW}Source not found.. skipping.${COLOR_RESET}"
  proceedop=false
 fi
 if $proceedop
 then
  if $inlineoperation
  then
    $operationcmd "$inlineoperationprefix" "$sourcepath" "$normgameinstall"
  else
    opresult=$($operationcmd "$sourcepath")
    echo -e "${COLOR_GREEN}Done.${COLOR_RESET}"
    [ -n "$opresult" ] && while read -r opline ; do echo "    > $opline" ; done<<<"$opresult"
  fi
 fi
}

function recover_libs () {
 libtype="$1"
 selecteddrive=""
 [ -n "$2" ] && selecteddrive="$2"
 selectedlibs=""
 while read -r file
 do
  file="${file% type *}" ; file="/${file#* on /}" ; [ "$file" = "/" ] && file=""
  if [ -f "$file/$CACHE_FLAG_FILE" ]
  then
   while read -r steamlibpath
   do
    [ -z "$steamlibpath" ] && break
    libpath="$file/$steamlibpath"
    if ! [ "$libtype" = "all" ] && ! $overridelibvalidation
    then
     2>/dev/null validatesteamlib "$libpath" false false
     [ "$libtype" = "src" ] && ! $validsourcelib && continue
     [ "$libtype" = "dst" ] && ! $validsteamlib && continue
     [ "$libtype" = "rdy" ] && ! $preparedsteamlib && continue
     [ "$libtype" = "usd" ] && ! $usedsteamlib && continue
    fi
    selectedlibs="$selectedlibs$libpath\n"
   done<<<"$(cat "$file/$CACHE_FLAG_FILE")"
  fi
 done<<<"$(mount | grep "$selecteddrive type ")"
 # if no drive was selected, also include the passed and active libraries (if not already considered)
 if [ -z "$selecteddrive" ]
 then
  while read -r libpath
  do
    { echo -e "$selectedlibs" | grep -q ^"$libpath"$ ;} && continue
    if ! [ "$libtype" = "all" ] && ! $overridelibvalidation
    then
     2>/dev/null validatesteamlib "$libpath" false false
     [ "$libtype" = "src" ] && ! $validsourcelib && continue
     [ "$libtype" = "dst" ] && ! $validsteamlib && continue
     [ "$libtype" = "rdy" ] && ! $preparedsteamlib && continue
     [ "$libtype" = "usd" ] && ! $usedsteamlib && continue
    fi
    selectedlibs="$selectedlibs$libpath\n"
  done<<<"$(echo -e "$activelibs\n$passedlibs")"
 fi
 echo -e "${selectedlibs%\\n}"
}
function recover_source_libs () {
 recover_libs "src" "$1"
}
function recover_destination_libs () {
 recover_libs "dst" "$1"
}
function recover_ready_libs () {
 recover_libs "rdy" "$1"
}
function recover_used_libs () {
 recover_libs "usd" "$1"
}
# shellcheck disable=SC2120 # `recover_all_libs` can be called with no parameters
function recover_all_libs () {
 recover_libs "all" "$1"
}

function addactivelib () {
 prefix="$1"
 librarypath="$2"
 normgameinstall="$3"
 
 if ! { echo -e "$selactivesteamlibs" | grep -q ^"$librarypath"$ ;}
 then
  selactivesteamlibs="$selactivesteamlibs\n$librarypath"
  selactivesteamlibs="${selactivesteamlibs#\\n}"
  echo -e "${COLOR_GREEN} Done adding.${COLOR_RESET}"
 else
  echo -e "${COLOR_YELLOW} Already added.${COLOR_RESET}"
 fi
}
function rmactivelib () {
 prefix="$1"
 librarypath="$2"
 normgameinstall="$3"
 
 if { echo -e "$selactivesteamlibs" | grep -q ^"$librarypath"$ ;}
 then
  reselectedlist=""
  while read -r sellib
  do
   reselectedlist="$reselectedlist$sellib\n"
  done<<<"$(echo -e "$selactivesteamlibs" | grep -v ^"$librarypath"$)"
  selactivesteamlibs="${reselectedlist%\\n}"
  echo -e "${COLOR_GREEN} Done.${COLOR_RESET}"
 else
  echo -e "${COLOR_YELLOW} Not in list.${COLOR_RESET}"
 fi
}

function checkselactivelibs () {
 echo -e "${COLOR_HMAGENTA}List of selected Active Steam Libs${COLOR_RESET}"
 echo -e "$selactivesteamlibs"
 echo
}

function issteamrunning () {
 steam_pid=$(cat "$STEAM_PID_PATH")
 [ -n "$steam_pid" ] && steampid_status=$(eval "$STEAMPID_CHECKER")
 if [ -n "$steampid_status" ]
 then
   return 0 #true
 fi
 return 1 #false
}
function stoponsteamrunning () {
 ! $PROCEED_STEAM_RUNNING && return 0 #variable set on false, return true to stop
 overridestop="i" #values (i=steam inactive, y/a=active but proceed, n/*=active don't proceed)
 echo -ne " ${COLOR_YELLOW}Steam process WILL REQUIRE TO BE MANUALLY RESTARTED after this operation ... ${COLOR_RESET}"
 echo -ne "${COLOR_HRESET}Proceed anyway? [y/n] ${COLOR_RESET}"
 read -r -N 1 overridestop </dev/tty
 ! [ "$overridestop" = "y" ] && echo -ne "${COLOR_RED} ... User decided not to continue ... ${COLOR_RESET}" && return 0 #user denied proceed
 echo ""
 return 1 #validation passed, don't stop.
}
function resetsteam () {
 rmsteamlibcfg=true
 [ ! -f "$steamconfigpath" ] && echo -e "No config file found under ${COLOR_HBLUE}$steamconfigpath${COLOR_RESET} ... ${COLOR_HRED}Aborting.${COLOR_RESET}" && return 1
 [ ! -f "$steamlibconfigpath" ] && echo -e "No config file found under ${COLOR_HBLUE}$steamlibconfigpath${COLOR_RESET} ... ${COLOR_HYELLOW}No need to remove it.${COLOR_RESET}" && rmsteamlibcfg=false
 steamproceed="i" #values (i=steam inactive, y/a=active but proceed, n/*=active don't proceed)
 if issteamrunning
 then
  echo -ne "${COLOR_YELLOW}Steam process running with PID $steam_pid ... May require to be restarted ... ${COLOR_RESET}"
  echo -ne "${COLOR_HRESET}Proceed anyway? [y/n] ${COLOR_RESET}"
  read -r -N 1 steamproceed </dev/tty
  ! [ "$steamproceed" = "y" ] && echo -e "${COLOR_RED} ... User decided not to continue ... ${COLOR_RESET}" && return 1
  echo ""
 fi
 
 # prepare libraries
 readytoswapstub=""
 finalactivelibs="$STEAM_BASE_LIB_PATH" #preinitialized with local steam lib (will always be active)
 steamlibcount=0
 while read -r steamlib
 do
  echo -e "${COLOR_BLUE}Validating Steam library [${COLOR_MAGENTA}$steamlib${COLOR_BLUE}] : ${COLOR_RESET}"
  [ "$steamlib" = "$STEAM_BASE_LIB_PATH" ] && echo -e "${COLOR_YELLOW}Steam Base Lib [${COLOR_MAGENTA}$steamlib${COLOR_YELLOW}]. Already added by default... Omiting.${COLOR_RESET}" && continue
  validatesteamlib "$steamlib" true true
  if $isflatpak
  then
   libinflatpak=false
   flatpak --user override "$STEAM_FLATPAKID" --filesystem="$steamlib" && libinflatpak=true
   if $libinflatpak
   then
    echo -e "${COLOR_GREEN}> Steam library [${COLOR_MAGENTA}$steamlib${COLOR_GREEN}] added to flatpak container correctly.${COLOR_RESET}"
   else
    echo -e "${COLOR_RED}> Steam library [${COLOR_MAGENTA}$steamlib${COLOR_RED}] failed to be added to flatpak container... If not already accessible in the same location will fail to load.${COLOR_RESET}"
   fi
  fi
  if $preparedsteamlib
  then
   steamlibcount=$(( steamlibcount + 1 ))
   readytoswapstub="$readytoswapstub\t\t\"BaseInstallFolder_$steamlibcount\"\t\"$steamlib\"\n"
   finalactivelibs="$finalactivelibs\n$steamlib"
   echo -e "${COLOR_GREEN}Steam library [${COLOR_MAGENTA}$steamlib${COLOR_GREEN}] ready to be used.${COLOR_RESET}"
  else 
   echo -e "${COLOR_RED}Steam library [${COLOR_MAGENTA}$steamlib${COLOR_RED}] not ready to be used.${COLOR_RESET}"
  fi
 done<<<"$(echo -e "$selactivesteamlibs")"
 readytoswapstub="${readytoswapstub%\\n}"

 if [ "$activelibs" = "$finalactivelibs" ]
 then
  echo -e "\n${COLOR_YELLOW}> Active libraries remain the same, nothing to change in steam config files.${COLOR_RESET}"
 else
  echo -e "\n${COLOR_YELLOW}-- Stub to be added to [${COLOR_HBLUE}$steamconfigpath${COLOR_YELLOW}] -------------------------------${COLOR_RESET}"
  echo -e "$readytoswapstub"
  echo -e "${COLOR_YELLOW}----------------------------------------------- ${COLOR_RED}*this will replace any current custom libraries${COLOR_YELLOW} ----${COLOR_RESET}\n"
  steamproceed="n"
  echo -ne "${COLOR_HRESET}Continue with modification? (This will close Steam if opened) [y/n] ${COLOR_RESET}"
  read -r -N 1 steamproceed </dev/tty
  [ ! "$steamproceed" = "y" ] && echo -e "${COLOR_RED} ... User decided not to continue ... ${COLOR_RESET}" && return 1
  echo -e "\n"
  
  stop_steam || return 1
  echo ""
  
  # backup config and generate a new one adding libraries
  echo -e "${COLOR_BLUE}Working on config files${COLOR_RESET}"
  echo -n "Generating new config file ... "
  cptimestamp="$(date +%Y%m%d_%H%M%S)"
  tmpcfgpath="/tmp/config.vdf.$cptimestamp"
  touch "$tmpcfgpath"
  passedsteamtag=false
  insteamsection=false
  addlibspending=true
  while read -r cfgline
  do
   [[ "$cfgline" == *"\"BaseInstallFolder_"* ]] && continue
   if $addlibspending
   then
    if $insteamsection
    then
     steamliblines="$(echo -e "$readytoswapstub" | grep -v ^$)"
     echo "$steamliblines" >> "$tmpcfgpath"
     addlibspending=false
    fi
    $passedsteamtag && [[ "$cfgline" =~ ^[[:space:]]*"{"[[:space:]]*$ ]] && insteamsection=true
    [[ "$cfgline" =~ ^[[:space:]]*"\"Steam\""[[:space:]]*$ ]] && passedsteamtag=true
   fi
   echo "$cfgline" >> "$tmpcfgpath"
  done <<<"$(cat """$steamconfigpath""")"
  echo -e "${COLOR_GREEN}Done${COLOR_RESET}"
 
  # backup config and move new one in position
  echo -ne "Backing up current config under [${COLOR_HBLUE}$steamconfigpath${COLOR_MAGENTA}.bak.$cptimestamp${COLOR_RESET}] ... "
  mv "$steamconfigpath" "$steamconfigpath.bak.$cptimestamp"
  echo -e "${COLOR_GREEN}Done${COLOR_RESET}"
  echo -ne "Backing up current library references under [${COLOR_HBLUE}$steamlibconfigpath${COLOR_MAGENTA}.bak.$cptimestamp${COLOR_RESET}] ... "
  $rmsteamlibcfg && mv "$steamlibconfigpath" "$steamlibconfigpath.bak.$cptimestamp" && echo -e "${COLOR_GREEN}Done${COLOR_RESET}" || echo -e "${COLOR_YELLOW}No file to backup${COLOR_RESET}"
  echo -ne "Moving generated config to [${COLOR_HBLUE}$steamconfigpath${COLOR_RESET}] ... "
  mv "$tmpcfgpath" "$steamconfigpath"
  echo -e "${COLOR_GREEN}""Done""${COLOR_RESET}"
  echo ""
 fi
 
 # start steam if requested
 echo -e "${COLOR_BLUE}Finishing${COLOR_RESET}"
 echo -ne "${COLOR_HRESET}Start Steam? [y/n] ${COLOR_RESET}"
 read -r -N 1 steamproceed </dev/tty
 [ ! "$steamproceed" = "y" ] && echo -e "${COLOR_RESET} ... User decided not to start steam. ${COLOR_HYELLOW}Steam will have to be manually started.${COLOR_RESET}" && return 1
 echo ""
 
 start_steam || return 1
}

function stop_steam () {
 # check Steam status and ensure it's closed before proceeding
 echo -e "${COLOR_BLUE}Checking for Steam${COLOR_RESET}"
 if issteamrunning
 then
  echo -e "Steam process running with PID ""${COLOR_MAGENTA}""$steam_pid""${COLOR_RESET}"" ..."
  echo -n "Killing steam process ."
  retries=0
  while [ -n "$steampid_status" ]
  do
   [[ $retries -gt $STEAMPID_RETRIES ]] && steampid_status="fail" && break
   retries=$(( retries + 1 ))
   if $isflatpak
   then
    1>/dev/null 2>/dev/null flatpak kill "$STEAM_FLATPAKID"
   else
    1>/dev/null 2>/dev/null kill "$steam_pid"
   fi
   sleep $STEAMPID_HEARTBEAT
   steampid_status=$(eval "$STEAMPID_CHECKER") 
   echo -n "."
  done
  [ -n "$steampid_status" ] && echo -e "${COLOR_RED}Failed${COLOR_RESET} ... ${COLOR_RED}Aborting.${COLOR_RESET}" && return 1
  echo -e "${COLOR_GREEN}Killed${COLOR_RESET} ... ${COLOR_GREEN}Ready to Proceed.${COLOR_RESET}"
 else
  echo -e "Steam is not running ... ${COLOR_GREEN}Ready to Proceed.${COLOR_RESET}\n"
 fi
 return 0
}
function start_steam () {
 issteamrunning && echo -e "${COLOR_GREEN}Steam is already running.${COLOR_RESET}" && return 0
 echo -n "Starting steam ..."
 sleep $STEAMPID_HEARTBEAT
 if $isflatpak
 then
  1>/dev/null 2>/dev/null nohup flatpak --user run "$STEAM_FLATPAKID" & disown
 else
  1>/dev/null 2>/dev/null nohup steam & disown
 fi
 sleep $STEAMPID_HEARTBEAT
 ! issteamrunning && echo -e "${COLOR_RESET}Failed to start Steam. ${COLOR_HYELLOW}Steam will have to be manually started.${COLOR_RESET}" && return 1
 echo -e "${COLOR_GREEN}Steam Started${COLOR_RESET}"
 return 0
}
function restart_steam () {
 stop_steam && start_steam && return 0 || return 1
}

function load_cl_repos () {
 repocache="/tmp/steamlibmanager.repos.tmp"
 echo -e "${COLOR_HCYAN}Retrieving Repos ...${COLOR_RESET}"
 if [ -f "$repocache" ]
 then
  repoloadedts=$(stat -c %Z "$repocache") ; nowts=$(date +%s) ; repoloadedago=$((nowts - repoloadedts))
  if [[ $RELOAD_REPOS_EVERY -gt $repoloadedago ]]
  then
   compatlayrepos="$(cat "$repocache")"
   compatlaynames=""
   while read -r repoline
   do
    compatlaynames="$compatlaynames\n$(echo "$repoline" | awk -F'|' '{print $1}' | xargs)"
   done<<<"$(echo -e "$compatlayrepos")"
   compatlaynames="${compatlaynames#\\n}"
   echo -e "${COLOR_CYAN} > Reloaded from Cache [created $(formattime "$repoloadedago") ago].${COLOR_RESET}"
   return 0
  fi
 fi
 releasesurls="ProtonGE https://api.github.com/repos/GloriousEggroll/proton-ge-custom/releases\nBoxtron https://api.github.com/repos/dreamer/boxtron/releases\nRoberta https://api.github.com/repos/dreamer/roberta/releases"
 compatlayrepos=""
 compatlaynames=""
 sep=""
 pcount=0
 while read -r project releasesurl
 do
  echo -e "${COLOR_CYAN} > Retrieving project $project ... $releasesurl${COLOR_RESET}"
  while read -r pline
  do
   case $pcount in
    '0')
     [[ "$pline" =~ "\"tag_name\"" ]] || continue
     sep="$sep$project " #add project name to release (to ensure differentiation)
     descriptor=true
     ;;
    '1')
     [[ "$pline" =~ "\"name\"" ]] && [[ "$pline" =~ ".tar." ]] || continue
     sep=" : "
     descriptor=true
     ;;
    '2')
     [[ "$pline" =~ "\"size\"" ]] || continue
     sep=" | "
     descriptor=false
     ;;
    '3')
     [[ "$pline" =~ "\"browser_download_url\"" ]] || continue
     sep=" | "
     descriptor=false
     ;;
   esac
   pline="$(echo "$pline" | awk -F": " '{print $2}')" ; pline=${pline%,*} ; pline=${pline%\"*} ; pline=${pline#*\"} #get the relevant info
   $descriptor && compatlaynames="$compatlaynames$sep$pline"
   compatlayrepos="$compatlayrepos$sep$pline"
   pcount=$((pcount + 1))
   [[ $pcount -eq 4 ]] && sep="\n" && pcount=0 #reset counter for next line
  done<<<"$(curl -s "$releasesurl" | grep -E "\"tag_name\":|\"name\":|\"size\":|\"browser_download_url\":")"
 done<<<"$(echo -e "$releasesurls")"
 echo "$compatlayrepos" > "$repocache"
 return 0
}
function dlcompattool () {
 clpath="$STEAM_COMPATIBILITYTOOLS_PATH"
 if ! [ -d "$clpath" ]
 then
  clexists=false
  echo -ne "${COLOR_YELLOW}Steam Compatibility Tools path [${COLOR_MAGENTA}$clpath${COLOR_YELLOW}] do not exist ... ${COLOR_RESET}"
  mkdir -p "$clpath" 
  [ -d "$clpath" ] && clexists=true
  ! $clexists && echo -e "${COLOR_RED}Not created... Aborting.${COLOR_RESET}" && return 1
  echo -e "${COLOR_GREEN}Created.${COLOR_RESET}"
 fi
 
 load_cl_repos
 
 echo -e "${COLOR_BLUE}Select Compatibility Tools to download and install (${COLOR_HBLUE}press [Esc] when done${COLOR_BLUE}): ${COLOR_RESET}"
 clrepos=""
 while true
 do
  onelinemenu clname "Select Compatibility Tools to install" "$(echo -e "$compatlaynames")" true
  if [ -n "$clname" ] 
  then
   if ( echo -e "$clrepos" | grep -q "$clname"  )
   then
    clrepos="$(echo -e "$clrepos" | grep -v "$clname")"
    echo -e "${COLOR_YELLOW}> Already selected Compatibility Tool ${COLOR_HMAGENTA}$clname${COLOR_YELLOW}. Deselecting.${COLOR_RESET}"
   else
    clrepo="$(echo -e "$compatlayrepos" | grep "$clname")"
    clrepos="$clrepos\n$clrepo"
    echo -e "${COLOR_GREEN}> Selecting ${COLOR_HMAGENTA}$clname${COLOR_GREEN}.${COLOR_RESET}"
   fi
  else
   echo -e "${COLOR_GREEN}Done selecting Compatibility Tools.${COLOR_RESET}"
   break
  fi
 done
 [ -z "$clrepos" ] && echo -e "${COLOR_RED}No Compatibility Tool selected... aborting.${COLOR_RESET}" && return 0
 echo ""

 while read -r clrepo
 do
  [ -z "$clrepo" ] && continue
  clname="$(echo "$clrepo" | awk -F'|' '{print $1}' | xargs)"
  clsize="$(echo "$clrepo" | awk -F'|' '{print $2}' | xargs)"
  clurl="$(echo "$clrepo" | awk -F'|' '{print $3}' | xargs)"
  clfile="$(basename "$clurl")"
  clfoldername="${clfile%.tar*}"
  echo -e "${COLOR_HBLUE}Installing $clname:${COLOR_RESET}"
  [ -z "$clfile" ] && echo -e "${COLOR_RED}> No file determined.. Aborting${COLOR_RESET}" && continue
  removeprev=false
  if [ -e "$clpath/$clfoldername" ]
  then
   echo -ne "${COLOR_YELLOW}> Compatibility Tool already in place, continuing will reinstall [${COLOR_BLUE}$clfile${COLOR_YELLOW}]${COLOR_RESET} ... Continue? [y/n] "
   read -r -N 1 clremove </dev/tty
   [ ! "$clremove" = "y" ] && echo -e "... ${COLOR_YELLOW}Omiting.${COLOR_RESET}" && continue
   echo -e "... ${COLOR_GREEN}Accepted.${COLOR_RESET}"
   removeprev=true
  fi
  failed=false
  downloadfile "${COLOR_YELLOW}> Downloading [${COLOR_BLUE}$clfile${COLOR_YELLOW}] ... ${COLOR_RESET}" "$((clsize /1024))" "$clurl" "$clpath/$clfile" true || failed=true
  $failed && echo -e "${COLOR_RED}> Download failed.. Omiting [${COLOR_BLUE}$clname${COLOR_RED}]${COLOR_RESET}" && continue
  if $removeprev
  then
   echo -ne "${COLOR_YELLOW}> Removing previously installed folder [${COLOR_BLUE}$clfoldername${COLOR_YELLOW}]${COLOR_RESET} ... "
   rm -r "$clpath/${clfoldername:?}" || failed=true
   $failed && echo -e "${COLOR_RED}Failed${COLOR_RESET}" && continue
   echo -e "${COLOR_GREEN}Done${COLOR_RESET}"
  fi
  decompresstar "${COLOR_YELLOW}> Decompressing [${COLOR_BLUE}$clfile${COLOR_YELLOW}] ... ${COLOR_RESET}" "$clpath/$clfile" "$clpath" || failed=true
  $failed && echo -e "${COLOR_RED}> Decompression failed.. Omiting [${COLOR_BLUE}$clname${COLOR_RED}]${COLOR_RESET}" && continue
  echo -e "${COLOR_GREEN}> [${COLOR_HBLUE}$clname${COLOR_GREEN}] installed${COLOR_RESET}"
  echo -ne "${COLOR_YELLOW}> Removing source file [${COLOR_BLUE}$clfile${COLOR_YELLOW}]${COLOR_RESET} ... "
  rm "$clpath/$clfile" || failed=true
  $failed && echo -e "${COLOR_RED}Failed${COLOR_RESET}" && continue
  echo -e "${COLOR_GREEN}Done${COLOR_RESET}"
  echo ""
 done<<<"$(echo -e "$clrepos")"

 # restart steam if requested
 echo -e "${COLOR_BLUE}Finishing${COLOR_RESET}"
 echo -e "${COLOR_YELLOW}Steam requires to restart in order for new Compatibility Tools to load${COLOR_RESET}"
 echo -ne "${COLOR_HRESET}Restart Steam? [y/n] ${COLOR_RESET}"
 read -r -N 1 steamproceed
 [ ! "$steamproceed" = "y" ] && echo -e "${COLOR_RESET} ... User decided not to start steam. ${COLOR_HYELLOW}Steam will have to be manually restarted.${COLOR_RESET}" && return 0
 echo ""
 restart_steam || return 1
 return 0
}
function downloadfile () {
 prefix="$1"
 size="$2"
 dlurl="$3"
 dlpath="$4"
 forcedl=false
 [ "$5" = "true" ] && forcedl=true
 
 curlparams=""

 if $forcedl
 then
  [ -e "$dlpath" ] && echo "Target already exists.. Trying to continue" >&2 && curlparams="-C -"
 else
  [ -e "$dlpath" ] && echo "Target already exists.. aborting" >&2 && return 1 
 fi
 [ ! -d "$(dirname "$dlpath")" ] && echo "No destination folder for $dlpath.. aborting" >&2 && return 1

 # $5=$size
 # $6=$dlurl
 # $7=$dlpath
 backgroundcmd="2>/dev/null curl -sL $curlparams \"\$6\" --output \"\$7\""
 totalcmd="echo \$((\"\$5\"))"
 progresscmd="2>/dev/null du -d0 \"\$7\" | tail -1 | awk '{print \$1}'"
 
 progressbar "$prefix" "$backgroundcmd" "$totalcmd" "$progresscmd" "$size" "$dlurl" "$dlpath" || return 1
 return 0
}
function decompresstar () {
 prefix="$1"
 tarfile="$2"
 dstdir="$3"
 
 [ ! -d "$(dirname "$dstdir")" ] && echo "No destination folder for $dstdir.. aborting" >&2 && return 1

 fileline="$(file "$tarfile")"
 tarparams=""
 if [[ "$fileline" =~ "gzip compressed data" ]]
 then
  tarparams="-xf"
  untarsize="$(gzip -l "$tarfile" | tail -1 | awk '{print $2}')"
 elif [[ "$fileline" =~ "XZ compressed data" ]] 
 then
  tarparams="-xJf"
  untarsize="$(tsz="$(xz -l "$tarfile" | tail -1 | awk '{print $5$6}')" && formatunits_i "${tsz%B}")"
 fi
 [ -z "$tarparams" ] && echo "Unable to determine compression type of $tarfile.. aborting" >&2 && return 1

 # $5=$untarsize
 # $6=$tarfile
 # $7=$dstdir
 backgroundcmd="2>/dev/null tar -xf \"\$6\" --directory \"\$7\""
 totalcmd="echo \$((\"\$5\" / 1024))"
 progresscmd="2>/dev/null du -d0 \"\$7\" | tail -1 | awk '{print \$1}'"
 
 progressbar "$prefix" "$backgroundcmd" "$totalcmd" "$progresscmd" "$untarsize" "$tarfile" "$dstdir" || return 1
 return 0
}
function compileshadercache () {
 prefix="$1"
 gameshaderpath="$2"
 normgameinstall="$3"
 
 # procedure determined from command run on Steam shader compiling
 #TODO check if this can be considered standard procedure:
 gameshadercache="$gameshaderpath/fozpipelinesv4/steam_pipeline_cache.foz"
 gameshaderwhitelist="$gameshaderpath/fozpipelinesv4/steam_pipeline_cache_whitelist"
 [ ! -f "$gameshadercache" ] && echo -e "${COLOR_YELLOW}No cache to compile... Skipping.${COLOR_RESET}" && return 0

 echo -ne "${COLOR_YELLOW}In progress...${COLOR_RESET}"

 shcompiled=false
 "$STEAM_SHADERCOMPILER" "$gameshadercache" --on-disk-validation-whitelist "$gameshaderwhitelist" &>/dev/null && shcompiled=true
 
 echo -ne "\r$prefix"
 if $shcompiled
 then
  echo -e "${COLOR_GREEN}Done.         ${COLOR_RESET}"
 else
  echo -e "${COLOR_RED}Failed.       ${COLOR_RESET}"
 fi
}
function enable_lib_asap () {
 prefix="$1"
 steamgamelib="$2"
 normgameinstall="$3"
 
 steamgamelib="$2"
 # prepare library library of game received
 addactivelib "$prefix" "$steamgamelib" || return 1
 resetsteam || return 1
 return 0
}
function lib_asap () {
 steamgamelib="$1"
 
 prefix="${COLOR_BLUE}Activating Game Library [${COLOR_HCYAN}$steamgamelib${COLOR_BLUE}] ... ${COLOR_RESET}"
 echo -ne "\r$prefix"
 enable_lib_asap "$prefix" "$steamgamelib" false || return 1
 return 0
}
function play_game_asap () {
 steamgameid="$1"
 steamgamelib="$2"
 
 lib_asap "$steamgamelib" || return 1
  
 # launch game received
 echo -e "${COLOR_BLUE}Launching game with call [${COLOR_HCYAN}steam://run/$steamgameid/${COLOR_BLUE}]${COLOR_BLUE}"
 if $isflatpak
 then
  flatpak --user run --command=steam "$STEAM_FLATPAKID" "steam://run/$steamgameid/"
 else
  steam "steam://run/$steamgameid/"
 fi
 return 0
}
function play_asap () {
 countparam=0
 steamgameid=""
 steamgamelib=""
 while read -r param
 do
  case $countparam in
   '0')
    steamgameid="$param"
    ;;
   '1')
    steamgamelib="$param"
    ;;
   *)
    break
    ;;
  esac
  countparam=$((countparam +1))
 done<<<"$(echo -e "$1")"
 
 play_game_asap "$steamgameid" "$steamgamelib" || return 1
 return 0
}

function steambrowser () {
 steam steam://openurl/$1 &>/dev/null
}

# script section
stty_regulate

# First validate steam libraries passed as parameters
passedlibs=""
if [ -n "$1" ]
then
 drawline
 echo -e "${COLOR_HMAGENTA}Evaluating Libraries passed as parameters:${COLOR_RESET}"
 for param in "$@"
 do
  if [ -d "$param" ]
  then
    paramlib="$(realpath "$param")"
    echo -e "${COLOR_BLUE}Parameter [${COLOR_CYAN}$param${COLOR_BLUE}] retrieved as [${COLOR_MAGENTA}$paramlib${COLOR_BLUE}]. Validating:${COLOR_RESET}"
  else
   echo -e "${COLOR_RED}Parameter [${COLOR_CYAN}$param${COLOR_RED}] not a directory. Omiting.\n----------------------------------------------${COLOR_RESET}" && continue
  fi
  validatesteamlib "$paramlib" false false
  if $validsourcelib
  then
   passedlibs="$passedlibs$paramlib\n" && echo -e "${COLOR_GREEN}Parameter Passed Library [${COLOR_CYAN}$paramlib${COLOR_GREEN}] accepted.\n----------------------------------------------${COLOR_RESET}" && continue
  else
   echo -e "${COLOR_RED}Parameter Passed Library [${COLOR_CYAN}$paramlib${COLOR_RED}] not accepted due to listed reasons.\n----------------------------------------------${COLOR_RESET}" 
  fi
 done
 echo -e "${COLOR_HMAGENTA}Libraries passed as parameters evaluated${COLOR_RESET}"
 passedlibs="${passedlibs%\\n}"
fi

steamproceed="i" #values (i=steam inactive, y/a=active but proceed, n/*=active don't proceed)
lastpreactivelibs="first"
activelibs=""
selactivesteamlibs="first"
while true
do
 
 # Each cycle, revalidate active libraries
 if [ -d "$STEAM_BASE_LIB_PATH/steamapps" ]
 then
  preactivelibs="$STEAM_BASE_LIB_PATH\n"
  if [ -f "$steamconfigpath" ]
  then 
   while read -r activelib
   do
    preactivelibs="$preactivelibs$activelib\n"
   done<<<"$(grep "BaseInstallFolder_" "$steamconfigpath" | awk -F'"' '{print $4}')"
  fi
  preactivelibs="${preactivelibs%\\n}"
  if [ ! "$preactivelibs" = "$lastpreactivelibs" ]
  then
   # Steam libraries if status changed
   [ ! "$lastpreactivelibs" = "first" ] && echo -e "${COLOR_HYELLOW}Steam Active Libs changed between runs, reevaluating...${COLOR_RESET}"
   lastpreactivelibs="$preactivelibs"
   drawline
   echo -e "${COLOR_HMAGENTA}Evaluating Steam Active Libraries:${COLOR_RESET}"
   activelibs=""
   while read -r activelib
   do
    if [ -d "$activelib" ]
    then
     echo -e "${COLOR_BLUE}Steam Library [${COLOR_CYAN}$activelib${COLOR_BLUE}] retrieved. Validating:${COLOR_RESET}"
    else
     echo -e "${COLOR_RED}Steam Active Library [${COLOR_CYAN}$activelib${COLOR_RED}] not a directory (will probably be removed by Steam on next boot). Omiting.\n----------------------------------------------${COLOR_RESET}" && continue
    fi
    validatesteamlib "$activelib" false false
    if $validsourcelib
    then
      activelibs="$activelibs$activelib\n" && echo -e "${COLOR_GREEN}Steam Active Library [${COLOR_CYAN}$activelib${COLOR_GREEN}] accepted.\n----------------------------------------------${COLOR_RESET}"
    else
      echo -e "${COLOR_RED}Steam Active Library [${COLOR_CYAN}$activelib${COLOR_RED}] not accepted due to listed reasons.\n----------------------------------------------${COLOR_RESET}" 
    fi
   done<<<"$(echo -e "$preactivelibs")"
   activelibs="${activelibs%\\n}"
   [ "$selactivesteamlibs" = "first" ] && selactivesteamlibs="$activelibs" && echo -e "${COLOR_HBLUE}Steam Active Libs set as Selected.${COLOR_RESET}"
   echo -e "${COLOR_HMAGENTA}Steam Active Libraries evaluated${COLOR_RESET}"
  fi
 else
  echo -e "${COLOR_HRED}Steam Base Library parameter STEAM_BASE_LIB_PATH is not set to usable library [${COLOR_HCYAN}$STEAM_BASE_LIB_PATH${COLOR_HRED}]. Please configure and retry. Aborting.${COLOR_RESET}" && exit 1
 fi

 drawline
 if issteamrunning
 then
  echo -ne "${COLOR_YELLOW}Steam process running with PID $steam_pid ... Precautions will be taken with active libraries ... ${COLOR_RESET}"
  if $ASK_IF_STEAM_RUNNING
  then
   if [ ! "$steamproceed" = "a" ]
   then
    steamproceed="n"
    echo -ne "${COLOR_HRESET}Proceed anyway? [y/n/a${COLOR_HRESET}(lways for this session)${COLOR_RESET}] ${COLOR_RESET}"
    read -r -N 1 steamproceed
    ! { [ "$steamproceed" = "y" ] || [ "$steamproceed" = "a" ] ; } && echo -e "${COLOR_RESET} ... User decided not to continue ... ${COLOR_RED}" "Exiting.${COLOR_RESET}" && exit 0
    echo -ne "${COLOR_GREEN} Accepted.${COLOR_RESET}"
   else
    echo -ne "${COLOR_GREEN} Accepted for this session.${COLOR_RESET}"
   fi
  fi
  echo ""
 else
  steamproceed="i"
 fi
 echo -e "${COLOR_HCYAN}Starting Operation${COLOR_RESET}"
 
 operationcmd=""
 operation=""
 oprequiresrc=false
 oprequiredst=false
 readydst=false
 notuseddst=false
 presetdsts=""
 opusesgame=false
 inlineoperation=false
 overridelibvalidation=false
 
 opt=""
 steamlib=""
 mainops=""
 # Main Menu Options list
 mainops="${mainops}\nPlay Game ASAP"
 mainops="${mainops}\nEnable Game Library ASAP"
 mainops="${mainops}\nEnable Library ASAP"
 mainops="${mainops}\nAnalyze Library"
 mainops="${mainops}\nValidate Library"
 mainops="${mainops}\nPrepare Library (for usage with Steam Linux)"
 mainops="${mainops}\nShow Games"
 $depcmdrsync && mainops="${mainops}\nCopy Games"
 $depcmdrsync && mainops="${mainops}\nCopy Only Game Data (no shaders or compatdata)"
 $depcmdrsync && mainops="${mainops}\nCopy Game Shaders Only"
 $depcmdrsync && mainops="${mainops}\nCopy Game CompatData Only"
 $depcmdrsync && mainops="${mainops}\nMove Games"
 mainops="${mainops}\nRemove/Delete Games"
 mainops="${mainops}\nRemove/Delete Game Shaders Only"
 mainops="${mainops}\nRemove/Delete Game CompatData Only"
 $depcmddirbrowser && mainops="${mainops}\nOpen Game Directory"
 $depcmddirbrowser && mainops="${mainops}\nOpen Game Shader Directory"
 $depcmddirbrowser && mainops="${mainops}\nOpen Game CompatData Directory"
 $depcmddirbrowser && mainops="${mainops}\nOpen Compatibility Tools Directory"
 $depcmddirbrowser && mainops="${mainops}\nOpen Main Steam Directory"
 mainops="${mainops}\nAdd Library to Selected Active Libs (for usage with Steam Linux)"
 mainops="${mainops}\nRemove Library from Selected Active Libs (for usage with Steam Linux)"
 mainops="${mainops}\nCheck current Selected Active Libs (for usage with Steam Linux)"
 mainops="${mainops}\nUse Steam with currently Selected Active Libs"
 $depcmdurlbrowser && mainops="${mainops}\nCheck ProtonDB Reports for Games"
 $depcmdurlbrowser && mainops="${mainops}\nCheck Steam Store Pages for Games"
 $depcmdurlbrowser && mainops="${mainops}\nCheck Steam Discussion Pages for Games"
 $depcmdurlbrowser && mainops="${mainops}\nCheck SteamDB entries for Games"
 $depcmdurlbrowser && mainops="${mainops}\nCheck PCGamingWiki for Games"
 $depcmdurlbrowser && mainops="${mainops}\nCheck Similar Games"
 $depcmdfossilize && mainops="${mainops}\nCompile Shaders"
 $depcmdcurl && mainops="${mainops}\nDownload Compatibility Tools"
 mainops="${mainops}\nRestart Steam"
 mainops="${mainops}\nExit/Quit"
 # /Main Menu Options list
 mainops="${mainops#\\n}"
  onelinemenu opt "Main Menu" "$(echo -e "$mainops")" true
  #preset values for all operations, only specific ones will be replaced in each one
  operationcmd=""
  operation=""
  oprequiresrc=false
  oprequiredst=false
  readydst=false
  notuseddst=false
  presetdsts=""
  opusesgame=false
  gameparamkeys=""
  gameopusedirs=false
  inlineoperation=false
  overridelibvalidation=false
  uniquegameselection=false
  case $opt in
   'Play Game ASAP')
    operationcmd="play_asap"
    operation="Preparing Steam to launch"
    oprequiresrc=true
    opusesgame=true
    gameparamkeys="t{GAMEID}\nt{GAMELIBPATH}"
    uniquegameselection=true
    ;;
   'Enable Game Library ASAP')
    operationcmd="lib_asap"
    operation="Enabling game library for"
    oprequiresrc=true
    opusesgame=true
    gameparamkeys="t{GAMELIBPATH}"
    uniquegameselection=true
    ;;
   'Enable Library ASAP')
    operationcmd="enable_lib_asap"
    operation="Enabling library"
    oprequiresrc=true
    inlineoperation=true
    ;;
   'Copy Games')
    operationcmd="copygamedata"
    operation="Copying"
    oprequiresrc=true
    oprequiredst=true
    notuseddst=true
    opusesgame=true
    gameparamkeys="compatdata/t{GAMEID}\nshadercache/t{GAMEID}\ntemp/t{GAMEID}\ndownloading/t{GAMEID}\ndownloading/state_t{GAMEID}_*.patch\nworkshop/content/t{GAMEID}\ncommon/t{GAMEINSTALLDIR}\nworkshop/appworkshop_t{GAMEID}.acf\nappmanifest_t{GAMEID}.acf"
    gameopusedirs=true
    inlineoperation=true
    ;;
   'Copy Only Game Data (no shaders or compatdata)')
    operationcmd="copygamedata"
    operation="Copying only"
    oprequiresrc=true
    oprequiredst=true
    notuseddst=true
    opusesgame=true
    gameparamkeys="temp/t{GAMEID}\ndownloading/t{GAMEID}\ndownloading/state_t{GAMEID}_*.patch\nworkshop/content/t{GAMEID}\ncommon/t{GAMEINSTALLDIR}\nworkshop/appworkshop_t{GAMEID}.acf\nappmanifest_t{GAMEID}.acf"
    gameopusedirs=true
    inlineoperation=true
    ;;
   'Copy Game Shaders Only')
    operationcmd="copygamedata"
    operation="Copying shaders of"
    oprequiresrc=true
    oprequiredst=true
    notuseddst=true
    opusesgame=true
    gameparamkeys="shadercache/t{GAMEID}"
    gameopusedirs=true
    inlineoperation=true
    ;;
   'Copy Game CompatData Only')
    operationcmd="copygamedata"
    operation="Copying compatdata of"
    oprequiresrc=true
    oprequiredst=true
    notuseddst=true
    opusesgame=true
    gameparamkeys="compatdata/t{GAMEID}"
    gameopusedirs=true
    inlineoperation=true
    ;;
   'Move Games')
    operationcmd="movegamedata"
    operation="Moving"
    oprequiresrc=true
    oprequiredst=true
    notuseddst=true
    opusesgame=true
    gameparamkeys="compatdata/t{GAMEID}\nshadercache/t{GAMEID}\ntemp/t{GAMEID}\ndownloading/t{GAMEID}\ndownloading/state_t{GAMEID}_*.patch\nworkshop/content/t{GAMEID}\ncommon/t{GAMEINSTALLDIR}\nworkshop/appworkshop_t{GAMEID}.acf\nappmanifest_t{GAMEID}.acf"
    gameopusedirs=true
    inlineoperation=true
    ;;
   'Remove/Delete Games')
    operationcmd="removegamedata"
    operation="Removing"
    oprequiredst=true
    notuseddst=true
    opusesgame=true
    gameparamkeys="compatdata/t{GAMEID}\nshadercache/t{GAMEID}\ntemp/t{GAMEID}\ndownloading/t{GAMEID}\ndownloading/state_t{GAMEID}_*.patch\nworkshop/content/t{GAMEID}\ncommon/t{GAMEINSTALLDIR}\nworkshop/appworkshop_t{GAMEID}.acf\nappmanifest_t{GAMEID}.acf"
    gameopusedirs=true
    inlineoperation=true
    ;;
   'Remove/Delete Game Shaders Only')
    operationcmd="removegamedata"
    operation="Removing shaders of"
    oprequiredst=true
    notuseddst=true
    opusesgame=true
    gameparamkeys="shadercache/t{GAMEID}"
    gameopusedirs=true
    inlineoperation=true
    ;;
   'Remove/Delete Game CompatData Only')
    operationcmd="removegamedata"
    operation="Removing compatdata of"
    oprequiredst=true
    notuseddst=true
    opusesgame=true
    gameparamkeys="compatdata/t{GAMEID}"
    gameopusedirs=true
    inlineoperation=true
    ;;
   'Show Games')
    operationcmd="du -h -s"
    operation="Showing"
    oprequiresrc=true
    opusesgame=true
    gameparamkeys="workshop/appworkshop_t{GAMEID}.acf\ncommon/t{GAMEINSTALLDIR}\nworkshop/content/t{GAMEID}\nappmanifest_t{GAMEID}.acf\ntemp/t{GAMEID}\ndownloading/t{GAMEID}\ndownloading/state_t{GAMEID}_*.patch\nshadercache/t{GAMEID}\ncompatdata/t{GAMEID}"
    gameopusedirs=true
    ;;
   'Analyze Library')
    operationcmd="analyzelib"
    operation="Analyzing"
    oprequiresrc=true
    ;;
   'Validate Library')
    operationcmd="validatelib"
    operation="Validating"
    oprequiresrc=true
    inlineoperation=true
    overridelibvalidation=true
    ;;
   'Prepare Library (for usage with Steam Linux)')
    operationcmd="preparelib"
    operation="Preparing"
    oprequiredst=true
    notuseddst=true
    inlineoperation=true
    overridelibvalidation=true
    ;;
   'Add Library to Selected Active Libs (for usage with Steam Linux)')
    operationcmd="addactivelib"
    operation="Adding to active libs"
    oprequiredst=true
    readydst=true
    presetdsts="$(recover_all_libs)"
    inlineoperation=true
    overridelibvalidation=true
    ;;
   'Remove Library from Selected Active Libs (for usage with Steam Linux)')
    operationcmd="rmactivelib"
    operation="Removing from active libs"
    oprequiredst=true
    readydst=true
    notuseddst=true
    presetdsts="$selactivesteamlibs"
    inlineoperation=true
    overridelibvalidation=true
    ;;
   'Check current Selected Active Libs (for usage with Steam Linux)')
    operationcmd="checkselactivelibs"
    operation="Checking active libs"
    inlineoperation=true
    overridelibvalidation=true
    ;;
   'Use Steam with currently Selected Active Libs')
    operationcmd="resetsteam"
    operation="Preparing Steam"
    inlineoperation=true
    overridelibvalidation=true
    ;;
   'Download Compatibility Tools')
    operationcmd="dlcompattool"
    operation="Downloading Compatibility Tools"
    inlineoperation=true
    overridelibvalidation=true
    ;;
   'Check ProtonDB Reports for Games')
    operationcmd="openlink"
    operation="Opening ProtonDB Report for"
    oprequiresrc=true
    notuseddst=true
    opusesgame=true
    gameparamkeys="https://www.protondb.com/app/t{GAMEID}"
    inlineoperation=true
    ;;
   'Check Steam Store Pages for Games')
    operationcmd="openlink"
    operation="Opening Store Page for"
    oprequiresrc=true
    notuseddst=true
    opusesgame=true
    gameparamkeys="https://store.steampowered.com/app/t{GAMEID}"
    inlineoperation=true
    ;;
   'Check Steam Discussion Pages for Games')
    operationcmd="openlink"
    operation="Opening Discussion Page for"
    oprequiresrc=true
    notuseddst=true
    opusesgame=true
    gameparamkeys="https://steamcommunity.com/app/t{GAMEID}/discussions/"
    inlineoperation=true
    ;;
   'Check SteamDB entries for Games')
    operationcmd="openlink"
    operation="Opening SteamDB Entry for"
    oprequiresrc=true
    notuseddst=true
    opusesgame=true
    gameparamkeys="https://steamdb.info/app/t{GAMEID}"
    inlineoperation=true
    ;;
   'Check PCGamingWiki for Games')
    operationcmd="openlink"
    operation="Opening PCGamingWiki for"
    oprequiresrc=true
    notuseddst=true
    opusesgame=true
    gameparamkeys="https://pcgamingwiki.com/api/appid.php?appid=t{GAMEID}&utm_source=SteamDB"
    inlineoperation=true
    ;;
   'Check Similar Games')
    operationcmd="openlink"
    operation="Opening PCGamingWiki for"
    oprequiresrc=true
    notuseddst=true
    opusesgame=true
    gameparamkeys="https://store.steampowered.com/recommended/morelike/app/t{GAMEID}/"
    inlineoperation=true
    ;;
   'Open Game Directory')
    operationcmd="opendir"
    operation="Opening Install Directory of"
    oprequiresrc=true
    notuseddst=true
    opusesgame=true
    gameparamkeys="common/t{GAMEINSTALLDIR}"
    gameopusedirs=true
    inlineoperation=true
    ;;
   'Open Game Shader Directory')
    operationcmd="opendir"
    operation="Opening Shader Directory of"
    oprequiresrc=true
    notuseddst=true
    opusesgame=true
    gameparamkeys="shadercache/t{GAMEID}"
    gameopusedirs=true
    inlineoperation=true
    ;;
   'Open Game CompatData Directory')
    operationcmd="opendir"
    operation="Opening CompatData Directory of"
    oprequiresrc=true
    notuseddst=true
    opusesgame=true
    gameparamkeys="compatdata/t{GAMEID}"
    gameopusedirs=true
    inlineoperation=true
    ;;
   'Open Compatibility Tools Directory')
    operationcmd="opencompattooldir"
    operation="Opening Compatibility Tools Directory"
    inlineoperation=true
    overridelibvalidation=true
    ;;
   'Open Main Steam Directory')
    operationcmd="opensteambasedir"
    operation="Opening Main Steam Directory"
    inlineoperation=true
    overridelibvalidation=true
    ;;
   'Compile Shaders')
    operationcmd="compileshadercache"
    operation="Compiling shaders of"
    oprequiredst=true
    notuseddst=true
    opusesgame=true
    gameparamkeys="shadercache/t{GAMEID}"
    gameopusedirs=true
    inlineoperation=true
    ;;
   'Restart Steam')
    operationcmd="restart_steam"
    operation="Restarting Steam"
    inlineoperation=true
    overridelibvalidation=true
    ;;
   'Exit/Quit'|'')
    echo -e 'Exiting per user request.'
    exit 0
    ;;
  esac
 echo -e "Operation [${COLOR_HMAGENTA}$opt${COLOR_RESET}] selected"
 echo ""
 
 library_from=""
 library_to=""
 selecteddrive=""
 avsrclibs=""
 multisources=false
 singledest=true
 onlyrepeated=false
 canmultidst=false && ! $oprequiresrc && $opusesgame && canmultidst=true
 if $oprequiresrc
 then
  echo -e "${COLOR_BLUE}Select source library:${COLOR_RESET}"
  additionallibs=""
  [ -n "$passedlibs" ] && additionallibs="${additionallibs}Parameter Passed Libs\n"
  [ -n "$activelibs" ] && additionallibs="${additionallibs}Steam Active Libs\n"
  [ -n "$selactivesteamlibs" ] && additionallibs="${additionallibs}Selected Active Libs\n"
  additionallibs="${additionallibs}Repeated Games\n"
  additionallibs="${additionallibs%\\n}"
  onelinemenu opt "Select Collection or Partition" "$([ -n "$additionallibs" ] && echo -e "$additionallibs" ; while read -r file ; do file="${file% type *}" ; file="/${file#* on /}" ; [ -f "$file/$CACHE_FLAG_FILE" ] && echo "$file"; done<<<"$(mount)")" "$opusesgame"
  if [ -z "$opt" ] || [ "$opt" = "Repeated Games" ]
  then
   multisources=true
   [ -z "$opt" ] && echo -e "${COLOR_HYELLOW}No partition selected, all games will be shown${COLOR_RESET}" 
   [ "$opt" = "Repeated Games" ] && onlyrepeated=true && echo -e "${COLOR_HYELLOW}Only repeated games will be shown${COLOR_RESET}"
   echo ""
   echo -ne "Loading Libs...\r"
   avsrclibs="$(recover_source_libs)"
  fi
 fi
 if $oprequiresrc && ! $multisources
 then
  case "$opt" in
   'Parameter Passed Libs'|'Steam Active Libs'|'Selected Active Libs')
    case "$opt" in
     'Parameter Passed Libs')
     specificlibs="$passedlibs"
     ;;
     'Steam Active Libs')
      specificlibs="$activelibs"
     ;;
     'Selected Active Libs')
      specificlibs="$selactivesteamlibs"
     ;;
    esac
    onelinemenu steamlib "Select Library" "$(echo -e "$specificlibs" ; echo "Repeated Games")" "$opusesgame"
    if [ -z "$steamlib" ] || [ "$steamlib" = "Repeated Games" ]
    then
     multisources=true
     [ -z "$steamlib" ] && echo -e "${COLOR_HYELLOW}No library selected, all games from $opt will be shown${COLOR_RESET}"
     [ "$steamlib" = "Repeated Games" ] && onlyrepeated=true && echo -e "${COLOR_HYELLOW}Only repeated games from $opt will be shown${COLOR_RESET}"
     echo ""
     avsrclibs="$specificlibs"
    else
     library_from="$steamlib"
    fi
   ;;
   *)
    selecteddrive="$opt"
    echo -e "Partition [${COLOR_HBLUE}$selecteddrive${COLOR_RESET}] selected"
    onelinemenu steamlib "Select Library" "$(cat "$selecteddrive/$CACHE_FLAG_FILE" ; echo "[ Repeated Games ]")" "$opusesgame"
    if [ -z "$steamlib" ] || [ "$steamlib" = "[ Repeated Games ]" ]
    then
     multisources=true
     [ -z "$steamlib" ] && echo -e "${COLOR_HYELLOW}No library selected, all games from [${COLOR_HBLUE}$selecteddrive${COLOR_HYELLOW}] will be shown${COLOR_RESET}"
     [ "$steamlib" = "[ Repeated Games ]" ] && onlyrepeated=true && echo -e "${COLOR_HYELLOW}Only repeated games from [${COLOR_HBLUE}$selecteddrive${COLOR_HYELLOW}] will be shown${COLOR_RESET}"
     echo ""
     echo -ne "Loading Libs...\r"
     avsrclibs="$(recover_source_libs "$selecteddrive")"
    else
     echo -e "Library [${COLOR_HBLUE}$steamlib${COLOR_RESET}] selected"
     separatortrail="/"
     [ "$selecteddrive" = "/" ] && separatortrail=""
     library_from=$selecteddrive$separatortrail$steamlib
    fi
   ;;
  esac
 fi
 if $oprequiresrc && ! $multisources && [ -n "$library_from" ]
 then
  echo -e "${COLOR_HRESET}Full Library Source Path: ${COLOR_HMAGENTA}$library_from${COLOR_RESET}"
  echo ""
  [ ! -d "$library_from/steamapps" ] && echo -e "${COLOR_RED}Selected source is not a library... aborting.${COLOR_RESET}" && continue
  if ! $overridelibvalidation
  then
   2>/dev/null validatesteamlib "$library_from" false false
   ! $validsourcelib && echo -e "${COLOR_RED}Selected library is not a valid source yet, please prepare it first... aborting.${COLOR_RESET}" && continue
  fi
  $opusesgame && [ ! "$(ls "$library_from"/steamapps/appmanifest_*.acf 2>/dev/null)" ] && echo -e "${COLOR_RED}Selected source library is empty... aborting.${COLOR_RESET}" && continue
 fi
 
 if $oprequiredst
 then
  echo -e "${COLOR_BLUE}Select destination library: ${COLOR_RESET}"
  if [ -z "$presetdsts" ]
  then
   additionallibs=""
   [ -n "$passedlibs" ] && additionallibs="${additionallibs}Parameter Passed Libs\n"
   [ -n "$activelibs" ] && additionallibs="${additionallibs}Steam Active Libs\n"
   [ -n "$selactivesteamlibs" ] && additionallibs="${additionallibs}Selected Active Libs\n"
   $canmultidst && additionallibs="${additionallibs}Repeated Games\n"
   additionallibs="${additionallibs%\\n}"
   onelinemenu opt "Select Collection or Partition" "$(echo -e "$additionallibs" ; while read -r file ; do file="${file% type *}" ; file="/${file#* on /}" ; [ -f "$file/$CACHE_FLAG_FILE" ] && echo "$file"; done<<<"$(mount)")" "$canmultidst"
   if [ -z "$opt" ] || [ "$opt" = "Repeated Games" ]
   then
    multisources=true
    singledest=false
    [ -z "$opt" ] && echo -e "${COLOR_HYELLOW}No partition selected, all games will be shown${COLOR_RESET}" 
    [ "$opt" = "Repeated Games" ] && onlyrepeated=true && echo -e "${COLOR_HYELLOW}Only repeated games will be shown${COLOR_RESET}"
    echo ""
    echo -ne "Loading Libs...\r"
    avsrclibs="$(recover_source_libs)"
   fi
   if $oprequiredst && $singledest
   then
    case "$opt" in
     'Parameter Passed Libs'|'Steam Active Libs'|'Selected Active Libs')
      case "$opt" in
       'Parameter Passed Libs')
       specificlibs="$passedlibs"
       ;;
       'Steam Active Libs')
        specificlibs="$activelibs"
       ;;
       'Selected Active Libs')
        specificlibs="$selactivesteamlibs"
       ;;
      esac
      onelinemenu steamlib "Select Library" "$(echo -e "$specificlibs" ; $canmultidst && echo "Repeated Games")" "$canmultidst"
      if [ -z "$steamlib" ] || [ "$steamlib" = "Repeated Games" ]
      then
       multisources=true
       singledest=false
       [ -z "$steamlib" ] && echo -e "${COLOR_HYELLOW}No library selected, all games from $opt will be shown${COLOR_RESET}"
       [ "$steamlib" = "Repeated Games" ] && onlyrepeated=true && echo -e "${COLOR_HYELLOW}Only repeated games from $opt will be shown${COLOR_RESET}"
       echo ""
       avsrclibs="$specificlibs"
      else
       library_to="$steamlib"
      fi
     ;;
     *)
      selecteddrive="$opt"
      echo -e "Partition [${COLOR_HBLUE}$selecteddrive${COLOR_RESET}] selected"
      onelinemenu steamlib "Select Library" "$(cat "$selecteddrive/$CACHE_FLAG_FILE" ; $canmultidst && echo "[ Repeated Games ]")" "$canmultidst"
      if [ -z "$steamlib" ] || [ "$steamlib" = "[ Repeated Games ]" ]
      then
       multisources=true
       [ -z "$steamlib" ] && echo -e "${COLOR_HYELLOW}No library selected, all games from [${COLOR_HBLUE}$selecteddrive${COLOR_HYELLOW}] will be shown${COLOR_RESET}"
       [ "$steamlib" = "[ Repeated Games ]" ] && onlyrepeated=true && echo -e "${COLOR_HYELLOW}Only repeated games from [${COLOR_HBLUE}$selecteddrive${COLOR_HYELLOW}] will be shown${COLOR_RESET}"
       echo ""
       echo -ne "Loading Libs...\r"
       avsrclibs="$(recover_source_libs "$selecteddrive")"
      else
       echo -e "Library [${COLOR_HBLUE}$steamlib${COLOR_RESET}] selected"
       separatortrail="/"
       [ "$selecteddrive" = "/" ] && separatortrail=""
       library_to=$selecteddrive$separatortrail$steamlib
      fi
     ;;
    esac
   fi
  else
    onelinemenu steamlib "Select Library" "$(echo -e "$presetdsts")" false
    library_to="$steamlib"
  fi
 fi
 if $oprequiredst
 then
  if [ -n "$library_to" ]
  then
   echo -e "Full Library Destination Path: ${COLOR_HMAGENTA}$library_to${COLOR_RESET}"
   echo ""
   [ ! -d "$library_to/steamapps" ] && echo -e "${COLOR_RED}Selected destination is not a library... aborting.${COLOR_RESET}" && continue
   [ "$library_to/steamapps" = "$library_from/steamapps" ] && echo -e "${COLOR_RED}Selected destination is the same as source... aborting.${COLOR_RESET}" && continue
   if ! $overridelibvalidation
   then
    2>/dev/null validatesteamlib "$library_to" false false
    ! $validsteamlib && echo -e "${COLOR_RED}Selected library is not a valid destination yet, please prepare it first... aborting.${COLOR_RESET}" && continue
    $readydst && ! $preparedsteamlib && echo -e "${COLOR_RED}Selected library is not a prepared destination yet, please prepare it first... aborting.${COLOR_RESET}" && continue
    $notuseddst && $usedsteamlib && issteamrunning && echo -ne "${COLOR_RED}Selected library is currently being used by steam.${COLOR_RESET}" && stoponsteamrunning && echo -e "${COLOR_RED}Please close steam before using... aborting.${COLOR_RESET}" && continue
   fi
   ! $oprequiresrc && $opusesgame && [ ! "$(ls "$library_to"/steamapps/appmanifest_*.acf 2>/dev/null)" ] && echo -e "${COLOR_RED}Selected destination library is empty... aborting.${COLOR_RESET}" && continue
  else
   ! $canmultidst && echo -e "${COLOR_RED}No selected destination library... aborting.${COLOR_RESET}" && continue
  fi
 fi

 if $opusesgame
 then
  echo -ne "Loading Games...\r"
  if $multisources
  then
   # list all available games from "$avsrc"
   availablegames="$(while read -r libpath ; do if [ -d "$libpath/steamapps" ]; then [ "$(ls "$libpath"/steamapps/appmanifest_*.acf 2>/dev/null)" ] && for gamemanifest in "$libpath"/steamapps/appmanifest_*.acf ; do gameid=$(basename "$gamemanifest") ; gameid=${gameid/appmanifest_/} ; gameid=${gameid/.acf/} ; gamename=$(grep '"name"' "$gamemanifest" | head -1 | awk -F '"' '{print $4}') ; echo "$gameid - $gamename | lib:$libpath" ; done ; fi ; done<<<"$(echo -e "$avsrclibs")" )"
   if $onlyrepeated
   then
    # list only repeated games from  "$availablegames"
    orderedavailablegames="$( echo "$availablegames" | sort -k1 )" #if sorted, simply counting the repeated lines should be enough
    availablegames="$(while read -r availablegame ; do prefixid="$(echo "$availablegame" | awk -F " - " '{print $1}' )" ; printed=false && [ "$prefixid" = "$lastprefixid" ] && { [ -n "$lastavailablegame" ] && echo "$lastavailablegame" ; lastavailablegame="" ; } && echo "$availablegame" && printed=true ; $printed && lastavailablegame="" || lastavailablegame="$availablegame" ; lastprefixid="$prefixid" ; done <<<"$orderedavailablegames" )"
   fi
  else
   # list only games from selected source
   srclibrary="$library_from"
   [ -z "$srclibrary" ] && srclibrary="$library_to"
   availablegames="$(for gamemanifest in "$srclibrary"/steamapps/appmanifest_*.acf ; do gameid=$(basename "$gamemanifest") ; gameid=${gameid/appmanifest_/} ; gameid=${gameid/.acf/} ; gamename=$(grep '"name"' "$gamemanifest" | head -1 | awk -F '"' '{print $4}') ; echo "$gameid - $gamename" ; done)"
  fi
  echo -ne "Sorting Games...\r"
  [ -z "$availablegames" ] && echo -e "${COLOR_RED}Selected collection doesn't contain any games to use as source... aborting.${COLOR_RESET}" && continue
  availablegames="$( echo "$availablegames" | sort -k2 )"
  
  echo -ne "${COLOR_BLUE}Select game"
  $uniquegameselection || echo -ne "s"
  if $oprequiresrc
  then
   echo -ne " to source operation"
  elif $oprequiredst
  then
   echo -ne " to apply operation"
  fi
  $uniquegameselection || echo -ne " (${COLOR_HBLUE}press [Esc] when done${COLOR_BLUE}): "
  echo -e "${COLOR_RESET}"

  selectedgames=""
  while true
  do
   selectedgame=""
   onelinemenu selectedgame "Select a game" "$(echo -e "$availablegames")"
   if [ -n "$selectedgame" ] 
   then
    if ( echo -e "$selectedgames" | grep -q "$selectedgame"  )
    then
     selectedgames="$(echo -e "$selectedgames" | grep -v "$selectedgame")"
     echo -e "${COLOR_YELLOW}> Already selected game ${COLOR_HMAGENTA}$selectedgame${COLOR_YELLOW}. Deselecting.${COLOR_RESET}"
    else
     selectedgames="$selectedgames\n$selectedgame"
     echo -e "${COLOR_GREEN}> Selecting ${COLOR_HMAGENTA}$selectedgame${COLOR_GREEN}.${COLOR_RESET}"
    fi
   else
    echo -e "${COLOR_GREEN}Done selecting games.${COLOR_RESET}"
    break
   fi
   $uniquegameselection && break #if this option is selected return, directly continue with first result
  done
  [ -z "$selectedgames" ] && echo -e "${COLOR_RED}No game selected... aborting.${COLOR_RESET}" && continue
  echo ""
  
  gamelibpath=""
  if $oprequiresrc
  then
    ! $multisources && gamelibpath="$library_from" #if multisources, this will be set later
  else
    gamelibpath="$library_to"
  fi
  confirmed="n"
  while read -r currentgame
  do
   [ -z "$currentgame" ] && continue
   currentgame="${currentgame//\\r/\\\\r}" ; #escape \r from game names
   if $multisources
   then
    gamelibpath="${currentgame##*" | lib:"}"
    if $oprequiresrc
    then
     $oprequiredst && [ "$library_to/steamapps" = "$gamelibpath/steamapps" ] && echo -e "${COLOR_RED}Game selected [${COLOR_MAGENTA}$currentgame${COLOR_RED}] has same source as destination... Canceling.${COLOR_RESET}" && continue
    elif $canmultidst
    then
     #TODO remove unecessary validations
     if [ -n "$gamelibpath" ]
     then
      # revalidate destination library one last time
      [ ! -d "$gamelibpath/steamapps" ] && echo -e "${COLOR_RED}Destination [${COLOR_MAGENTA}$gamelibpath${COLOR_RED}] is not a library... aborting.${COLOR_RESET}" && continue
      [ "$gamelibpath/steamapps" = "$library_from/steamapps" ] && echo -e "${COLOR_RED}Destination [${COLOR_MAGENTA}$gamelibpath${COLOR_RED}] is the same as source... aborting.${COLOR_RESET}" && continue
      if ! $overridelibvalidation
      then
       2>/dev/null validatesteamlib "$gamelibpath" false false
       ! $validsteamlib && echo -e "${COLOR_RED}Destination [${COLOR_MAGENTA}$gamelibpath${COLOR_RED}] is not a valid destination yet, please prepare it first... aborting.${COLOR_RESET}" && continue
       $readydst && ! $preparedsteamlib && echo -e "${COLOR_RED}Destination [${COLOR_MAGENTA}$gamelibpath${COLOR_RED}] is not a prepared destination yet, please prepare it first... aborting.${COLOR_RESET}" && continue
       $notuseddst && $usedsteamlib && issteamrunning && echo -ne "${COLOR_RED}Destination [${COLOR_MAGENTA}$gamelibpath${COLOR_RED}] is currently being used by steam.${COLOR_RESET}" && stoponsteamrunning && echo -e "${COLOR_RED}Please close steam before using... aborting.${COLOR_RESET}" && continue
      fi
      ! $oprequiresrc && $opusesgame && [ ! "$(ls "$gamelibpath"/steamapps/appmanifest_*.acf 2>/dev/null)" ] && echo -e "${COLOR_RED}Destination [${COLOR_MAGENTA}$gamelibpath${COLOR_RED}] library is empty... aborting.${COLOR_RESET}" && continue
     else
      echo -e "${COLOR_RED}No selected destination library... aborting.${COLOR_RESET}" && continue
     fi
    fi
   fi
   gameid="${currentgame%%" - "*}"
   [ ! -d "$gamelibpath" ] && echo -e "${COLOR_RED}Game library [${COLOR_MAGENTA}$gamelibpath${COLOR_RED}] not available anymore... Omiting.${COLOR_RESET}" && continue
   [ ! -f "$gamelibpath"/steamapps/appmanifest_"$gameid".acf ] && echo -e "${COLOR_RED}Game selected [${COLOR_MAGENTA}$currentgame${COLOR_RED}] not available anymore... Omiting.${COLOR_RESET}" && continue
   gameinstalldir=$(grep "\"installdir\"" "$gamelibpath"/steamapps/appmanifest_"$gameid".acf | awk -F '"' '{print $4}')
   if [ -z "$gameinstalldir" ]
   then
    echo -e "${COLOR_RED} Manifest [${COLOR_MAGENTA}$gamelibpath/steamapps/appmanifest_$gameid.acf${COLOR_RED}] doesn't declare a game install directory. Cannot proceed!${COLOR_RESET}"
    continue
   fi
   #------ validate if installdir is used by another game in library
   normgameinstall=false
   [[ "$(grep "\"installdir\"" "$gamelibpath"/steamapps/appmanifest_*.acf | awk -F '"' '{print $4}' | grep -c ^"$gameinstalldir"$)" -gt 1 ]] && normgameinstall=true
   $normgameinstall && echo -e "${COLOR_YELLOW}Game directory [${COLOR_BLUE}$gamelibpath/common/${COLOR_MAGENTA}$gameinstalldir${COLOR_YELLOW}] used by other games. Precautions will be taken.${COLOR_RESET}"
   #------
   echo -ne "${COLOR_HBLUE}$operation${COLOR_RESET} game ${COLOR_HBLUE}$currentgame${COLOR_RESET}"
   if $oprequiresrc && $oprequiredst
   then
     echo -ne " from ${COLOR_HBLUE}$gamelibpath${COLOR_RESET} to ${COLOR_HBLUE}$library_to${COLOR_RESET}" 
   else
     echo -ne " on ${COLOR_HBLUE}$gamelibpath${COLOR_RESET}" 
   fi
   if [ "$confirmed" = "a" ]
   then
    echo -e "${COLOR_GREEN} Procedure previously confirmed.${COLOR_RESET}"
   elif [ "$confirmed" = "c" ]
   then
    echo -e "${COLOR_RED} Procedure previously cancelled.${COLOR_RESET}"
    continue
   else
    echo -ne "${COLOR_HRESET} Confirm? [y/n/a${COLOR_RESET}(ll)${COLOR_HRESET}/c${COLOR_RESET}(ancel all)${COLOR_HRESET}]${COLOR_RESET}"
    read -r -N 1 -p " > " confirmed </dev/tty
    echo ""
    if [ "$confirmed" = "a" ]
    then
     echo -ne "${COLOR_GREEN}Procedure confirmed for all following games selected... You won't be prompted with a selection until the operation is finished.${COLOR_RESET}"
    elif [ "$confirmed" = "c" ]
    then
     echo -e "${COLOR_RED}Procedure cancelled for all following games selected... You won't be prompted with a selection until the operation is finished.${COLOR_RESET}"
     continue
    elif [ ! "$confirmed" = "y" ]
    then
     echo -e "${COLOR_RED}Procedure canceled.${COLOR_RESET}"
     continue
    else
     echo -ne "${COLOR_GREEN}Procedure confirmed.${COLOR_RESET}"
    fi
    echo ""
   fi
   
   echo -e "${COLOR_HRESET}Starting procedure:${COLOR_RESET}"
   trap_forcedgameopbreak #change forced exit for operation
   
   gameparams="${gameparamkeys//t\{GAMEINSTALLDIR\}/$gameinstalldir}"
   gameparams="${gameparams//t\{GAMEID\}/$gameid}"
   gameparams="${gameparams//t\{GAMELIBPATH\}/$gamelibpath}"

   if $gameopusedirs
   then
   while read -r gamepathkey
   do
    { [ ! "$confirmed" = "y" ] && [ ! "$confirmed" = "a" ] ; } && break #sanity check
    sourcepaths="$gamelibpath/steamapps/$gamepathkey"
    
    #normalize sourcepaths symbols for escaped usage with ls
    sourcepaths=${sourcepaths// /\\ }
    sourcepaths=${sourcepaths//\'/\\\'}
    sourcepaths=${sourcepaths//&/\\&}
    sourcepaths=${sourcepaths//;/\\;}
    sourcepaths=${sourcepaths//\(/\\\(}
    sourcepaths=${sourcepaths//)/\\)}
    
    while read -r sourcepath
    do
     { [ ! "$confirmed" = "y" ] && [ ! "$confirmed" = "a" ] ; } && break #sanity check
     [ -z "$sourcepath" ] && continue
     gamepath="${sourcepath/$gamelibpath\/steamapps\//}"
     if $oprequiresrc && $oprequiredst
     then
      destinationpath="$library_to/steamapps/$gamepath"
      operate_two_libs "$operation" "$gamepath" "$sourcepath" "$destinationpath" "$operationcmd" "$inlineoperation" "$normgameinstall"
     else
      operate_one_lib "$operation" "$gamepath" "$sourcepath" "$operationcmd" "$inlineoperation" "$normgameinstall"
     fi
    done <<<"$(2>/dev/null eval ""ls -1 -d "${sourcepaths}""")"
   done<<<"$(echo -e "$gameparams")"
   else
     trap_donothing #avoid process exit if interrupted by user
     #simply run the requested command with the replaced gameparams in its own shell
     (
      trap_forcedexit #use a exit trap to exit subshell
      "$operationcmd" "$gameparams"
     )
   fi
   trap_forcedexit #restore original trap
   echo -e "${COLOR_HRESET}Procedure Finished${COLOR_RESET}"
  done <<<"$(echo -e "$selectedgames")"
 else # ! $opusesgame
  normgameinstall=true #TODO check what to do with this variable in these cases
  trap_forcedgameopbreak #change forced exit for operation
  if $oprequiresrc
  then
   oplabel="$library_from"
   sourcepath="$library_from"
   if $oprequiredst
   then
    destinationpath="$library_to"
    operate_two_libs "$operation" "$oplabel" "$sourcepath" "$destinationpath" "$operationcmd" "$inlineoperation" "$normgameinstall"
   else
    operate_one_lib "$operation" "$oplabel" "$sourcepath" "$operationcmd" "$inlineoperation" "$normgameinstall"
   fi
  elif $oprequiredst # ! $opusesgame
  then
   oplabel="$library_to"
   destinationpath="$library_to"
   operate_one_lib "$operation" "$oplabel" "$destinationpath" "$operationcmd" "$inlineoperation" "$normgameinstall"
  else
   trap_donothing #avoid process exit if interrupted by user
   #no library is required at all, simply run the requested command in its own shell
   (
    trap_forcedexit #use a exit trap to exit subshell
    "$operationcmd" 
   )
  fi
  trap_forcedexit #restore original trap
 fi
 echo -e "${COLOR_HCYAN}Operation Finished${COLOR_RESET}"
done
exit 0
